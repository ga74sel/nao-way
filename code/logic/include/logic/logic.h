/**
 * @file logic.h
 *
 * @brief statemachine that combines localization, obstacledetection, behaviours and path planning
 *
 * @ingroup logic
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */


#ifndef LOGIC_H
#define LOGIC_H

#include <ros/ros.h>
#include <math.h>

#include <std_srvs/Empty.h>
#include <std_msgs/Bool.h>

#include <geometry_msgs/Pose2D.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_listener.h>

#include "planner_msgs/plan.h"              // planner srv
#include <planner_msgs/posestate.h>
#include <planner_msgs/path.h>

#include "walk_manager/WalkPose.h"          // walking
#include <head_manager/MoveJoints.h>        // head movement
#include <behaviour/StepHeight.h>           // step behaviour

#include <obstacle_msgs/changed.h>
#include <obstacle_msgs/detect.h>

namespace logic
{

/**
 * Implementation of a statemachine that control robot logic
 *
 * Repedatly excutes sense, plan act loop until the goal position is reached
 *
 */
class Logic
{

public:
    Logic();

    void update();

private:
    /**
    * @brief call service to clear_map
    */
    void clear_map();
    /**
    * @brief call service to localize robot
    */
    bool localize();
    /**
    * @brief call service to detect obstacles
    */
    void detect_obstacles();
    /**
    * @brief call service to generate new path
    */
    bool plan_path(geometry_msgs::Pose2D &goal);
    /**
    * @brief call service to walk to a specified goal
    */
    void walk_to(geometry_msgs::Pose2D &goal);
    /**
    * @brief call service to turn head
    */
    void turn_head(float yaw, float pitch, float time);


    /**
    * @brief approch object with id
    */
    void approach(int id);
    /**
    * @brief execute a step of height h
    */
    void step_up(double h);
    /**
    * @brief execute a step down of height h
    */
    void step_down(double h);
    /**
    * @brief execute a step over obstacle
    */
    void step_over();

    /// Lookup transformations
    void get_tf_camera_goal(tf::StampedTransform &transform);
    void get_tf_base_map(tf::StampedTransform &transform);

    /// AUXILIARY
    double norm(geometry_msgs::Pose2D &p);
    void intermediate_goal();
    void rotate_to_goal();

    /// CALLBACKS
    bool start_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool reset_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool cont_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool retry_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);

    void goal_reached_cb(const std_msgs::Bool::ConstPtr& msg);
    void behaviour_sub_cb(const std_msgs::Bool::ConstPtr& msg);
    void joint_state_head_cb(const sensor_msgs::JointState::ConstPtr& joint_state);
    void changed_obstacle_cb(const obstacle_msgs::changed::ConstPtr& msg);
    void timer_cb(const ros::TimerEvent&);

private:
    ros::NodeHandle nh;

    // States
    enum State {IDLE, LOCALIZE, SENSE, PLAN, ACT, COMPLETE_ACTION, ERROR_HANDLER, WAIT};
    State state, last_state;

    enum LState {INIT, ROTATE_2_GOAL, SEARCH_GOAL, WAIT_TO_DETECT, DETECT, LOCALIZATION_DONE};
    LState localize_state;

    enum BState {APPROACHING, WAIT_4_COMPLETION, STEP_UP_STATE, STEP_DOWN_STATE, STEP_OVER_STATE, FINISH};
    BState b_state, b_next_state;

    enum SState { WAIT_TO_SENSE, DO_SENSING, TURN, SENSING_DONE};
    SState s_state;

    // Behaviour
    enum Behaviour { WALK=planner_msgs::posestate::STATE_MOVE, STEP_UP=planner_msgs::posestate::STATE_APPROACH, STEP_OVER=planner_msgs::posestate::STATE_STEP };
    Behaviour behaviour_;
    double height;

    bool is_busy;
    bool retry, cont;
    bool is_localized;
    bool goal_reached;
    bool behaviour_success, behaviour_flag;


    geometry_msgs::Pose2D curr_goal;
    geometry_msgs::Pose2D head_state;

    // action counter
    int act_count;
    int plan_count;
    int turn_count;
    int sense_count;
    int rotate_direction;
    int obstacle_id;

    // FSM functions
    void state_transition();                    // main state machine
    bool localize_FSM();                        // localization state machine
    bool behaviour_FSM();                       // behaviour state machine
    bool sense_FSM();

    void switch_to_error_state();

    // Tf
    tf::TransformListener listener;
    tf::StampedTransform H_map_camera;

    // Timer
    ros::Timer timer;
    bool timer_flag;

    // Subscriber
    ros::Subscriber goal_reached_sub;           // walk_manager finished
    ros::Subscriber behaviour_sub;              // behaviour finished
    ros::Subscriber joint_state_sub;            // /jointstate
    ros::Subscriber changed_obstacle_sub;       // ???

    // Service Server
    ros::ServiceServer start_srv;               // start logic
    ros::ServiceServer reset_srv;               // reset logic
    ros::ServiceServer retry_srv;               // retry logic
    ros::ServiceServer cont_srv;

    // Service Client
    ros::ServiceClient localize_client;
    ros::ServiceClient sense_client;
    ros::ServiceClient plan_client;
    ros::ServiceClient clear_map_client;
    ros::ServiceClient clear_visualizer_client;

    // locomotion
    ros::ServiceClient walk_client;
    ros::ServiceClient turn_head_client;
    ros::ServiceClient stop_walk_client;
    // behaviours
    ros::ServiceClient approach_client;
    ros::ServiceClient step_up_client;
    ros::ServiceClient step_down_client;
    ros::ServiceClient step_over_client;
};

}

#endif // LOGIC_H
