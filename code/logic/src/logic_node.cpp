#include "ros/ros.h"
#include "logic/logic.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "logic_node");
    ROS_INFO_STREAM("logic_node...");

    logic::Logic logic;
    ros::Rate loop_rate(10);

    while(ros::ok())
    {
        logic.update();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
