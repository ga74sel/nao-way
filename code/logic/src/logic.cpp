#include "logic/logic.h"
#include <obstacle_msgs/clear.h>
#include <behaviour/Approach.h>

#define DETECTION_TIME  2.2
#define WALK_DISTANCE   0.2


using namespace logic;

Logic::Logic() : state(IDLE), is_localized(false), goal_reached(false)
{
    // Subscriber
    goal_reached_sub = nh.subscribe("/walk_manager/goal_reached", 1, &Logic::goal_reached_cb, this);
    joint_state_sub = nh.subscribe("/joint_states",1, &Logic::joint_state_head_cb, this);
    changed_obstacle_sub = nh.subscribe("/obstacledetector/changed",1, &Logic::changed_obstacle_cb, this);
    behaviour_sub = nh.subscribe("/behaviour/success", 1, &Logic::behaviour_sub_cb, this);

    // Service Server
    start_srv = nh.advertiseService("logic/start", &Logic::start_srv_cb, this);             // start logic
    reset_srv = nh.advertiseService("logic/reset", &Logic::reset_srv_cb, this);             // reset logic
    retry_srv = nh.advertiseService("logic/retry", &Logic::retry_srv_cb, this);             // retry logic
    cont_srv = nh.advertiseService("logic/cont", &Logic::cont_srv_cb, this);

    // Service Client
    localize_client = nh.serviceClient<std_srvs::Empty>("/localization/localize");
    sense_client = nh.serviceClient<obstacle_msgs::detect>("/obstacledetector/detect");
    clear_map_client = nh.serviceClient<obstacle_msgs::clear>("/obstacledetector/clear");
    plan_client = nh.serviceClient<planner_msgs::plan>("/planner1/plan");
    clear_visualizer_client = nh.serviceClient<std_srvs::Empty>("/visualizer/clear");
    // locomotion
    walk_client = nh.serviceClient<walk_manager::WalkPose>("/walk_manager/walk");
    turn_head_client = nh.serviceClient<head_manager::MoveJoints>("/move_joint");
    stop_walk_client = nh.serviceClient<std_srvs::Empty>("/stop_walk_srv");         // ???

    // behaviours
    approach_client = nh.serviceClient<behaviour::Approach>("/behaviour/approaching");
    step_up_client = nh.serviceClient<behaviour::StepHeight>("/behaviour/step_up");
    step_down_client = nh.serviceClient<behaviour::StepHeight>("/behaviour/step_down");
    step_over_client = nh.serviceClient<std_srvs::Empty>("/behaviour/step_over");

    ROS_INFO_STREAM("State: IDLE");

    // Timer
    timer = nh.createTimer(1, &Logic::timer_cb, this);
    timer_flag = false;

    // initialize control flags
    is_busy = false;
    retry = false;
    cont = false;

}


void Logic::update() {

    state_transition();
}



void Logic::state_transition() {


    switch(state) {

    case IDLE: {

        if(is_busy) {

            clear_map();
            is_localized = false;
            rotate_direction = 0;

            // zero head rotation
            turn_head(0.0, 0.0, 0.8);

            // set state
            state = LOCALIZE;
            localize_state = INIT;
            b_state = APPROACHING;
            s_state = WAIT_TO_SENSE;

            ROS_INFO_STREAM("State: LOCALIZE");
        }
      break;
    }

    case LOCALIZE: {

        /* Performs the whole localization procedure */
        bool success = localize_FSM();

        if(success) {
           is_localized = true;
           ROS_INFO_STREAM("Localization successful!");

           state = SENSE;
           ROS_INFO_STREAM("State: SENSE");

           // reset counter
           act_count = 0;
           sense_count = 0;
        }
      break;
    }

    case SENSE: {
        /*if(sense_FSM()) {
            state = PLAN;
            ROS_INFO_STREAM("State: PLAN");
        }*/
        if(timer_flag) {
            detect_obstacles();
            state = PLAN;
        }
            break;
    }

    case PLAN: {

        if(!timer_flag) {
            break;
        }

        plan_count++;
        if(plan_path(curr_goal)) {

            // reach goal marker?
            tf::StampedTransform transform;
            get_tf_base_map(transform);
            auto &o = transform.getOrigin();
            double d = sqrt( o.x()*o.x() + o.y()*o.y());
            ROS_INFO_STREAM("Dist to goal: " << d);
            //if(norm(curr_goal) < 0.001 || d < (0.40 + WALK_DISTANCE)) {
            if(d < (0.40 + WALK_DISTANCE)) {
                ROS_INFO_STREAM("Goal reached!");
                ROS_INFO_STREAM("State: INIT");
                state = IDLE;
                is_busy = false;
            }
            else {
                state = ACT;
                ROS_INFO_STREAM("State: ACT");

                // Walk to current goal
                intermediate_goal();
                ROS_INFO_STREAM("goal: " << curr_goal.x << ", " << curr_goal.y << ", " << curr_goal.theta);
                walk_to(curr_goal);
                ros::Duration(5).sleep();
                act_count++;
            }
        }
        else {                                  // planning failed
            if(plan_count < 2) {                // do sensing and plan again
                state = SENSE;
                ROS_INFO_STREAM("State: SENSE");
            }
            else {
                ROS_ERROR("Path planning failed!");
                state = SENSE;
                switch_to_error_state();
            }
        }
      break;
    }

    case ACT: {

        if(behaviour_ == WALK) {
            state = COMPLETE_ACTION;
            ROS_INFO_STREAM("State: COMPLETE_ACTION");
        }
        else {
            // deal STEP_OVER and STEP_UP in seperate State machine
            if(behaviour_FSM()) {
                last_state = LOCALIZE;
                state = WAIT;
            }
        }
      break;
    }

    case COMPLETE_ACTION: {

        if(goal_reached) {
            goal_reached = false;

            if(act_count == 1) {
                //state = LOCALIZE;
                last_state = LOCALIZE;
                ROS_INFO_STREAM("State: LOCALIZE");
            }
            else {
                last_state = SENSE;
                ROS_INFO_STREAM("State: SENSE");
            }
            state = WAIT;
        }
      break;
    }

    /* STATE: WAIT FOR USER INPUT */
    case WAIT: {
        //ROS_WARN_STREAM_THROTTLE(10, "pause");
        if(1) { // cont
            cont = false;
            state = last_state;
        }
      break;
    }

    case ERROR_HANDLER: {

        if(retry) {
            retry = false;
            state = last_state;
            ROS_WARN("Restart!");
        }
      break;
    }

    }

    timer_flag = false;
}



bool Logic::localize_FSM() {

    switch(localize_state) {

    case INIT: {

        // reset turn_count of head
        turn_count = 0;

        if(localize()) {
            localize_state = LOCALIZATION_DONE;
            ROS_INFO_STREAM(".....state: LOCALIZATION_DONE");
        }
        else {
            if(is_localized) {
                localize_state = ROTATE_2_GOAL;
                ROS_INFO_STREAM(".....state: ROTATE_2_GOAL");
            }
            else {
                localize_state = SEARCH_GOAL;
                ROS_INFO_STREAM(".....state: SEARCH_GOAL");
            }
        }
      break;
    }

    /* rotate the head to an existing goal */
    case ROTATE_2_GOAL: {

        // performs rotation logic for localization
        rotate_to_goal();

        localize_state = WAIT_TO_DETECT;
        ROS_INFO_STREAM(".....state: WAIT_TO_DETECT");
      break;
    }

    /* intermediate state */
    case WAIT_TO_DETECT: {
        if(timer_flag) {
            localize_state = DETECT;
            ROS_INFO_STREAM(".....state: DETECT");
        }
      break;
    }

    /* call localization function */
    case DETECT: {

        if(timer_flag) {
            if(localize()) {
                localize_state = LOCALIZATION_DONE;
                ROS_INFO_STREAM(".....state: LOCALIZATION_DONE");
            }
            else {
                localize_state = SEARCH_GOAL;
                ROS_INFO_STREAM(".....state: SEARCH_GOAL");
            }
        }
      break;
    }

    /* We haven't found the goal: search for goal actively! */
    case SEARCH_GOAL: {

        float d_yaw = (turn_count % 2) ? 0.15 : -0.15;

        turn_head(turn_count * d_yaw, 0, 1);
        turn_count++;
        localize_state = WAIT_TO_DETECT;

        if(turn_count == 10) {
            ROS_ERROR_STREAM("Localization failed!");
            switch_to_error_state();
        }

      break;
    }
    case LOCALIZATION_DONE: {

        // rotate back if required
        if(rotate_direction) {
            geometry_msgs::Pose2D rot_goal;
            rot_goal.x = rot_goal.y = 0;
            rot_goal.theta = -rotate_direction * M_PI/2;
            walk_to(rot_goal);
            rotate_direction = 0;
        }

        // reset head position
        turn_head(0, 0, 1.5);
        localize_state = INIT;
        return true;
    }

    }

    timer_flag = false;         // reset timer_flag
    return false;
}


bool Logic::behaviour_FSM() {

    switch(b_state) {

    case APPROACHING: {

        ROS_INFO_STREAM("-----State: APPROACHING");
        // approaching srv
        approach(obstacle_id);
        ros::Duration(1).sleep();

        // set b_state & b_next_state
        b_state = WAIT_4_COMPLETION;
        if(behaviour_ == STEP_OVER) {
            b_next_state = STEP_OVER_STATE;
            ROS_INFO_STREAM("-----next: STEP_OVER_STATE");
        }
        else {
            b_next_state = STEP_UP_STATE;
            ROS_INFO_STREAM("-----next: STEP_UP_STATE");
        }
        ROS_INFO_STREAM("(1)---state: WAIT_4_COMPLETION");

      break;
    }

    case WAIT_4_COMPLETION: {

        if(behaviour_flag)  {
            behaviour_flag = false;
            if(behaviour_success) {
                b_state = b_next_state;
            }
            else {
                ROS_ERROR("Action failed!");
                b_state = APPROACHING;
                state = LOCALIZE;               // redo localization
                switch_to_error_state();
            }
        }
      break;
    }

    case STEP_UP_STATE: {

        // call step_up srv
        step_up(height);

        b_state = WAIT_4_COMPLETION;
        b_next_state = STEP_DOWN_STATE;
        ROS_INFO_STREAM("-----next: STEP_DOWN_STATE");
        ROS_INFO_STREAM("-----state: WAIT_4_COMPLETION");
      break;
    }

    case STEP_DOWN_STATE: {

        // call step_up srv
        step_down(height);

        b_state = WAIT_4_COMPLETION;
        b_next_state = FINISH;
        ROS_INFO_STREAM("-----next: FINISH");
        ROS_INFO_STREAM("-----state: WAIT_4_COMPLETION");
      break;
    }

    case STEP_OVER_STATE: {

        // call step_over srv
        step_over();

        b_state = WAIT_4_COMPLETION;
        b_next_state = FINISH;
        ROS_INFO_STREAM("-----next: FINISH");
        ROS_INFO_STREAM("-----state: WAIT_4_COMPLETION");
      break;
    }

    case FINISH: {
        b_state = APPROACHING;
        ROS_INFO_STREAM("clear_map()");
        //clear_map();
        return true;
    }

    }
    return false;
}


bool Logic::sense_FSM() {

    switch(s_state) {

    case(WAIT_TO_SENSE): {
        if(timer_flag){
            s_state = DO_SENSING;
            ROS_INFO_STREAM("*****state: DO_SENSING");
        }
        break;
    }

    case(DO_SENSING): {
        if(timer_flag) {
            ROS_INFO_STREAM("start!");
            detect_obstacles();
            s_state = TURN;
            sense_count++;
            ROS_INFO_STREAM("*****state: TURN");
        }
      break;
    }

    case(TURN): {
        if(sense_count == 3){
            s_state = SENSING_DONE;
            turn_head(0, 0, 1);
            ros::Duration(1).sleep();
            ROS_INFO_STREAM("*****state: SENSING_DONE");
        }
        else {
            // turn head
            int flag = (sense_count==1) ? -1 : 1;
            turn_head(flag*0.3, 0.5, 0.5);
            s_state = WAIT_TO_SENSE;
            ROS_INFO_STREAM("*****state: WAIT_TO_SENSE");
        }
      break;
    }

    case(SENSING_DONE): {
        s_state = WAIT_TO_SENSE;
        sense_count = 0;
        return true;
      break;
    }

    }

    timer_flag = false;
    return false;
}


void Logic::switch_to_error_state() {

    localize_state = INIT;
    last_state = state;
    state = ERROR_HANDLER;
    b_state = APPROACHING;
    ROS_WARN("Switched to ERROR_STATE. Previous state can be retried!");
}


// -------------------
// AUXILIARY Functions
// -------------------

void Logic::rotate_to_goal() {

    rotate_direction = 0;

    // 1. get transformation /goal -> /CameraTop_optical_frame
    get_tf_camera_goal(H_map_camera);
    auto &t = H_map_camera.getOrigin();
    //ROS_INFO_STREAM("ogigin: " << t.x() << ", " << t.y() << ", " << t.z());

    // 2. rotate torso if required
    if(t.z() < 0) {
        rotate_direction = (t.x() < 0) ? 1 : -1;
        geometry_msgs::Pose2D rot_goal;
        rot_goal.x = rot_goal.y = 0;
        rot_goal.theta = rotate_direction * M_PI/2;

        walk_to(rot_goal);
        ros::Duration(5.0).sleep();
    }

    // 3. get transformation /goal -> /CameraTop_optical_frame
    get_tf_camera_goal(H_map_camera);
    auto &t2 = H_map_camera.getOrigin();

    // 4. determine rotation angle
    double yaw = -atan(t2.x() / t2.z());
    double pitch = atan(t2.y() / t2.z());
    ROS_INFO_STREAM("yaw: " << yaw << ", pitch " << pitch);

    // 3. rotate head
    turn_head(yaw, pitch, 0.7);
}

void Logic::intermediate_goal() {

    if(norm(curr_goal) > 1.5 * WALK_DISTANCE) {
        // walk only a distance of WALK_DISTANCE
        curr_goal.x *= (WALK_DISTANCE / norm(curr_goal));
        curr_goal.y *= (WALK_DISTANCE / norm(curr_goal));
    }
}

double Logic::norm(geometry_msgs::Pose2D &p){
    return sqrt(p.x * p.x + p.y * p.y);
}

void Logic::get_tf_camera_goal(tf::StampedTransform &transform) {
    try {
        listener.lookupTransform("/CameraTop_optical_frame", "/goal", ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("transform lookup failed!");
        return;
    }
}

void Logic::get_tf_base_map(tf::StampedTransform &transform) {
    try {
        listener.lookupTransform("/map", "/base_footprint", ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("transform lookup failed!");
        return;
    }
}

// --------------------------------------------------------------------------------------------


// -----------------------
// Service Client Function
// -----------------------

void Logic::clear_map() {
    obstacle_msgs::clear srv;
    clear_map_client.call(srv);
    std_srvs::Empty srv2;
    clear_visualizer_client.call(srv2);
}

bool Logic::localize() {
    std_srvs::Empty srv;
    return localize_client.call(srv);;
}

void Logic::detect_obstacles() {
    obstacle_msgs::detect srv;
    sense_client.call(srv);
}

bool Logic::plan_path(geometry_msgs::Pose2D &goal) {

    planner_msgs::plan srv;
    if(plan_client.call(srv)) {
        auto &curr_point = srv.response.path[1];
        goal = curr_point.pose;
        behaviour_ = (Behaviour)curr_point.state;
        obstacle_id = curr_point.id;

        for(int i=0; i<srv.response.path.size(); i++) {
            ROS_INFO_STREAM("behaviour " <<  (int)srv.response.path[i].state << " | " << (Behaviour) srv.response.path[i].state);
        }

        height = curr_point.height;
        ROS_INFO_STREAM("# BEHAVIOUR " << behaviour_ << "    h: " << height << " goal: " << goal);
        return true;
    }

    else {
        ROS_ERROR_STREAM("no plan!");
        return false;
    }
}

void Logic::turn_head(float yaw, float pitch, float time) {

    ros::service::waitForService("move_joint");
    head_manager::MoveJoints srv;
    srv.request.joint_names = { "HeadYaw", "HeadPitch" };
    srv.request.time_list = {time, time};
    srv.request.joint_angles = {yaw, pitch};
    srv.request.use_interpolation = 1;
    turn_head_client.call(srv);
}


void Logic::walk_to(geometry_msgs::Pose2D &goal) {
    walk_manager::WalkPose srv;
    srv.request.pose = goal;
    walk_client.call(srv);
}


/* Behaviours */
void Logic::approach(int id) {
    ROS_INFO_STREAM("before approach srv!");
    behaviour::Approach srv;
    srv.request.id = id;
    approach_client.call(srv);
    ROS_INFO_STREAM("after approach srv!");
}

void Logic::step_up(double h) {
    behaviour::StepHeight srv;
    srv.request.height.data = h;
    step_up_client.call(srv);
}

void Logic::step_down(double h) {
    behaviour::StepHeight srv;
    srv.request.height.data = h;
    step_down_client.call(srv);
}

void Logic::step_over() {
    std_srvs::Empty srv;
    step_over_client.call(srv);
}


// --------------------------------------------------------------------------------------------


// ---------
// CALLBACKS
// ---------

bool Logic::start_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {

    is_busy = true;
    return true;
}

bool Logic::reset_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {

    is_busy = false;
    state = IDLE;
    return true;
}

// TODO: remove later
bool Logic::cont_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {

    cont = true;
    return true;
}

bool Logic::retry_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    retry = true;
    return true;
}

// ------------------------

void Logic::joint_state_head_cb(const sensor_msgs::JointState::ConstPtr& joint_state) {

    head_state.x = joint_state->position[0]; // yaw
    head_state.y = joint_state->position[1]; // pitch
    //ROS_INFO_STREAM_THROTTLE(2, "head_state: " << head_state.x << ", "  << head_state.y);
}

void Logic::behaviour_sub_cb(const std_msgs::Bool::ConstPtr& msg) {
    behaviour_flag = true;
    behaviour_success = msg->data;
}

void Logic::goal_reached_cb(const std_msgs::Bool::ConstPtr& msg) {
    goal_reached = true;
}

void Logic::changed_obstacle_cb(const obstacle_msgs::changed::ConstPtr& msg) {
    if( is_busy ) {
        //std_srvs::Empty srv;
        //stop_walk_client.call(srv);
        // ROS_INFO_STREAM("(cb) Obstacle Changed!");
    }
}

void Logic::timer_cb(const ros::TimerEvent&) {
    timer_flag = true;
}
