#!/usr/bin/env python
import rospy
import time
import almath
import sys
from naoqi import ALProxy
from walk_manager.srv import *


class Walker(object):
    def __init__(self, robotIP, PORT):
        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)
        self.motionProxy.setMoveArmsEnabled(True, True)
        self.srv = rospy.Service("walk_manager/walk_slow", WalkPose, self.walk_handler)

    def walk_handler(self, req):
	print("walk_slow handler");

        taskList = self.motionProxy.getTaskList()
        for taskid in taskList:
            self.motionProxy.killTask(taskid[1])

        self.motionProxy.moveTo(req.pose.x, req.pose.y, req.pose.theta,
                [ ["MaxStepX", 0.01],
                  ["MaxStepY", 0.12],
	          ["MaxStepTheta", 0.4],
                  ["MaxStepFrequency", 0.6],
                  ["StepHeight", 0.02] ])

        return WalkPoseResponse()

if __name__ == '__main__':
    robotIP=str(sys.argv[1])
    PORT=int(sys.argv[2])

    rospy.init_node('walk_manager_node')
    walker = Walker(robotIP, PORT)

    rospy.spin()

