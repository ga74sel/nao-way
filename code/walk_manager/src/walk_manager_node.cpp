#include "ros/ros.h"

#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose2D.h>
#include <sensor_msgs/JointState.h>

#include <walk_manager/WalkPose.h>
#include <math.h>



/* Walker
 *
 * wrapper for the walking motion.
 * First rotate, then move straight to intermediate goal position.
 *
 * SRV: /walker_manager/move_to
 *
 */
class Walker {

public:

    Walker() : goal_reached(true), is_moving(false), move_count(0) {

        // Publisher
        goal_reached_pub = nh.advertise<std_msgs::Bool>("/walk_manager/goal_reached", 10);
        cmd_pose_pub = nh.advertise<geometry_msgs::Pose2D>("/cmd_pose", 10);

        // Subscriber
        joint_states_sub = nh.subscribe("/joint_states",1, &Walker::joint_state_cb, this);

        // Service Server
        move_to = nh.advertiseService("/walk_manager/walk", &Walker::move_to_cb, this);

        state = IDLE;
    }

    /* State machine
     *
     * (1) rotate
     * (2) move straight
     */
    void update() {

        switch(state) {

        case IDLE: {
            if(!goal_reached) {

                //if(goal.theta >  0.3) {
                if(1) {
                    state = ROTATE;
                    ROS_INFO_STREAM("State ROTATE: " << goal.theta);
                    msg.x = 0;
                    msg.y = 0;
                    msg.theta = goal.theta;
                    cmd_pose_pub.publish(msg);
                }
                else {
                    state = STRAIGHT;
                    ROS_INFO_STREAM("State STRAIGHT: " << goal.x << " " << goal.y);
                    msg.x = goal.x;
                    msg.y = goal.y;
                    msg.theta = goal.theta;
                    cmd_pose_pub.publish(msg);
                }
                is_moving=true;
                move_count=0;
             }
           break;
        }

        case ROTATE: {

            if(!is_moving) {
                state = STRAIGHT;
                ROS_INFO_STREAM("State STRAIGHT: " << goal.x << " " << goal.y);
                msg.x = sqrt(goal.x * goal.x + goal.y * goal.y);
                msg.y = 0;
                msg.theta = 0;
                cmd_pose_pub.publish(msg);

                is_moving=true;
                move_count=0;
            }
          break;
        }

        case STRAIGHT: {

            if(!is_moving) {
                state = IDLE;
                ROS_INFO_STREAM("State IDLE");
                goal_reached = true;

                std_msgs::Bool msg_done;
                msg_done.data = true;
                goal_reached_pub.publish(msg_done);
            }
          break;
        }

        }

    }

private:

    geometry_msgs::Pose2D goal;
    bool goal_reached;
    bool is_moving;
    int move_count;

    enum State {IDLE, ROTATE, STRAIGHT};
    State state;

    geometry_msgs::Pose2D msg;

    ros::NodeHandle nh;

    // Publisher
    ros::Publisher goal_reached_pub;            // publish when goal reached
    ros::Publisher cmd_pose_pub;                // walk msg

    // Subscriber
    ros::Subscriber joint_states_sub;

    // Service Server
    ros::ServiceServer move_to;


    // ---------
    // Callbacks
    // ---------

    bool move_to_cb(walk_manager::WalkPose::Request &req, walk_manager::WalkPose::Response &res) {

        goal = req.pose;
        goal_reached = false;
        ROS_INFO_STREAM("new goal: " << goal.x << ", " << goal.y << ", " << goal.theta);
        return true;
    }

    /* Checks if the robot is moving. */
    void joint_state_cb(const sensor_msgs::JointState::ConstPtr& msg) {

        for(int i=0; i<msg->effort.size(); ++i) {
            if(msg->effort[i] > 0.10) {
                is_moving = true;
                move_count = 0;
                return;
            }
        }
        move_count++;
        if(move_count > 6)
            is_moving = false;
      }

}; /* Walker */



// -----
// Main
// -----

int main(int argc, char** argv)
{
    ros::init(argc, argv, "walk_manager_node");
    ROS_INFO_STREAM("walk_manager_node...");

    Walker walker;
    ros::Rate loop_rate(5);

    while(ros::ok())
    {
        walker.update();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
