#!/usr/bin/env python
import rospy
import time
import almath
import sys
from naoqi import ALProxy
from head_manager.srv import *


class JointMotion(object):
    def __init__(self, robotIP, PORT):
        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)
        self.srv = rospy.Service('move_joint', MoveJoints, self.handler)

    def handler(self, req):
        taskList = self.motionProxy.getTaskList()
        for taskid in taskList:
            self.motionProxy.killTask(taskid[1])

        if req.use_interpolation == 1:
            self.motionProxy.post.angleInterpolation(req.joint_names, req.joint_angles, req.time_list, True)
        else:
            self.motionProxy.setAngles(req.joint_names, req.joint_angles, req.speed)
        return MoveJointsResponse()


if __name__ == '__main__':
    robotIP=str(sys.argv[1])
    PORT=int(sys.argv[2])

    rospy.init_node('head_manager_node')

    joint_mover = JointMotion(robotIP, PORT)
    rospy.spin()

