#ifndef GRIDMAP_2D_H_
#define GRIDMAP_2D_H_

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <geometry_msgs/Point.h>
#include <nav_msgs/OccupancyGrid.h>

#include <memory>

/*
 * Subsribe to obstacle_msgs and publishes a nav_msgs
 * that contains a 2D map of the current view
 * Information are stored as a cv::Mat array
 */

namespace gridmap_2d
{

class GridMap2D
{
public:
    /* create from given parameters */
    GridMap2D(float width, float heigth, float resolution, const std::string& frame);
    /* create from given nav_msgs */
    GridMap2D(const nav_msgs::OccupancyGridConstPtr& occupancy_grid);
    virtual ~GridMap2D();

    /* transform between pixel and world coordinates */
    void mapToWorld(unsigned int mx, unsigned int my, double& wx, double& wy) const;
    void worldToMapNoBounds(double wx, double wy, unsigned int& mx, unsigned int& my) const;
    bool worldToMap(double wx, double wy, unsigned int& mx, unsigned int& my) const;

    /* check if in bounds */
    bool inMapBounds(double wx, double wy) const;

    /* inflate by radius, stepable obstacles are not inflated */
    void inflateMap(double infl_rad_obstacle, double infl_rad_stepable);

    void clearMap();

    /* distance between points */
    inline double worldDist(unsigned x1, unsigned y1, unsigned x2, unsigned y2) {
            return worldDist(cv::Point(x1, y1), cv::Point(x2, y2));
    }

    inline double worldDist(const cv::Point& p1, const cv::Point& p2) {
            return GridMap2D::pointDist(p1, p2) * map_info.resolution;
    }

    static inline double pointDist(const cv::Point& p1, const cv::Point& p2) {
            return sqrt(pointDist2(p1, p2));
    }
    static inline double pointDist2(const cv::Point& p1, const cv::Point& p2) {
            return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y);
    }

    // returns distance (in m) at world coordinates <wx,wy> in m
    float distanceMapAt(double wx, double wy) const;

    // returns distance (in m) at map cell <mx, my> in m
    float distanceMapAtCell(unsigned int mx, unsigned int my) const;

    // returns map value at world coordinates <wx, wy>
    uchar binaryMapAt(double wx, double wy) const;

    // returns map value at map cell <mx, my>
    uchar binaryMapAtCell(unsigned int mx, unsigned int my) const;

    // returns map value at map cell <mx, my>
    uchar& binaryMapAtCell(unsigned int mx, unsigned int my);

    // returns the map value at map cell <mx, my> and the id
    uchar idMapAtCell(unsigned int mx, unsigned int my, int& id) const;

    // returns map value at map cell <mx, my> and the id
    uchar idMapAt(double wx, double wy, int &id) const;

    // return true if map is occupied
    bool isOccupiedAt(double wx, double wy) const;

    // return true if map is occupied at cell <mx, my>
    bool isOccupiedAtCell(unsigned int mx, unsigned int my) const;

    // initialize map from a ROS OccupancyGrid message
    void setMap(const nav_msgs::OccupancyGridConstPtr& grid_map, bool unknown_as_obstacle = false);

    // initialize from an existing cv::Map
    void setMap(const cv::Mat& binary_map);

    // set cost at world coordinates <wx, wy>
    void setMapCostAt(double wx, double wy, unsigned char cost, unsigned char id = 0);

    // set cost at map cell <mx, my>
    void setMapCostAtCell(double wx, double wy, unsigned char cost, unsigned char id = 0);

    // set cost for entire convex polygon 
    void setConvexPolygonCost(const std::vector<double>& wx_vec, const std::vector<double>& wy_vec, unsigned char cost, unsigned char id = 0);
    void setConvexPolygonCost(const std::vector<geometry_msgs::Point>& points, unsigned char cost, unsigned char id = 0);

    // converts back into a ROS nav_msgs::OccupancyGrid msg
    nav_msgs::OccupancyGrid toOccupancyGridMsg(float potential = 0.0f) const;

    // recalculate the internal distance map
    void updateDistanceMap();

    inline const nav_msgs::MapMetaData& getInfo() const {return map_info;}
    inline float getResolution() const {return map_info.resolution; }
    // returns the tf frame ID of the map
    inline const std::string getFrameID() const {return frame;}
    // return the cv::Mat distance image.
    const cv::Mat& distanceMap() const {return dist_map_obst;}
    // return the cv::Mat binary image.
    const cv::Mat& binaryMap() const {return map;}
    // return the size of the cv::Mat binary image
    inline const CvSize size() const {return map.size();}
    // return physical size of map
    inline const cv::Point2f worldSize() const { return map_info.resolution * cv::Point2f(map.size()); }

    const static uchar FREE = 255;          // char value for "free": 255
    const static uchar STEPABLE = 128;      // char value for "stepable": 128 - 254
    const static uchar OCCUPIED = 0;        // char value for "blocked": 0 - 128

private:
    cv::Mat map, inf_map;                   // cv images to store map
    cv::Mat dist_map_obst, dist_map_step;   // mat to store distance to obstacles
    nav_msgs::MapMetaData map_info;
    std::string frame;
};

typedef std::shared_ptr<GridMap2D> GridMap2DPtr;
typedef std::shared_ptr<const GridMap2D> GridMap2DConstPtr;

}

#endif
