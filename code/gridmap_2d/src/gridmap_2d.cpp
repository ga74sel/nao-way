#include "gridmap_2d/gridmap_2d.h"

#include <iostream>

using namespace gridmap_2d;

/* create from given parameters */
GridMap2D::GridMap2D(float width, float heigth, float resolution, const std::string& frame)
	: frame(frame)
{
    map_info.width = std::ceil(width / resolution);     // cells
    map_info.height = std::ceil(heigth / resolution);   // cells
    map_info.resolution = resolution;                   // m/cells
    map_info.origin.position.x = -0.5*width;
    map_info.origin.position.y = -0.5*heigth;

    map = cv::Mat(map_info.width, map_info.height, CV_8UC1);
    inf_map = cv::Mat(map.size(), CV_8UC1);
    dist_map_obst = cv::Mat(map.size(), CV_32FC1);
    dist_map_step = cv::Mat(map.size(), CV_32FC1);

    map.setTo( cv::Scalar(FREE) );
    inf_map.setTo( cv::Scalar(FREE) );
}

GridMap2D::GridMap2D(const nav_msgs::OccupancyGridConstPtr& occupancy_grid)
{
	setMap(occupancy_grid);
}

GridMap2D::~GridMap2D()
{
}

void GridMap2D::clearMap()
{
    map.setTo( cv::Scalar(FREE) );
}


void GridMap2D::updateDistanceMap() { 
    // binary map of all obstacles
    inf_map = map > (STEPABLE-1);
    cv::distanceTransform(inf_map, dist_map_obst, CV_DIST_L2, CV_DIST_MASK_PRECISE);

    inf_map = map < STEPABLE | map >= FREE;
    cv::distanceTransform(inf_map, dist_map_step, CV_DIST_L2, CV_DIST_MASK_PRECISE);


    // distance map now contains distance in meters:
    dist_map_obst = dist_map_obst * map_info.resolution;
    dist_map_step = dist_map_step * map_info.resolution;
}

void GridMap2D::setMap(const nav_msgs::OccupancyGridConstPtr& grid_map, bool unknown_as_obstacle) {
    map_info = grid_map->info;
    frame = grid_map->header.frame_id;
    // allocate map structs so that x/y in the world correspond to x/y in the image
    // (=> cv::Mat is rotated by 90 deg, because it's row-major!)
    map = cv::Mat(map_info.width, map_info.height, CV_8UC1);
    dist_map_obst = cv::Mat(map.size(), CV_32FC1);

    std::vector<signed char>::const_iterator mapDataIter = grid_map->data.begin();

    // iterate over map, store in image
    // (0,0) is lower left corner of OccupancyGrid
    for(unsigned int j = 0; j < map_info.height; ++j) {
        for(unsigned int i = 0; i < map_info.width; ++i) {
            if( *mapDataIter == 0 ) {
                map.at<uchar>(i,j) = FREE;
            } else {
                if (*mapDataIter >= 100 )
                    map.at<uchar>(i,j) = OCCUPIED + *mapDataIter - 100;
                else
                    map.at<uchar>(i,j) = STEPABLE + *mapDataIter - 50;
            }
            ++mapDataIter;
        }
    }
    updateDistanceMap();
}

nav_msgs::OccupancyGrid GridMap2D::toOccupancyGridMsg(float potential) const{
    nav_msgs::OccupancyGrid msg;
    msg.header.frame_id = frame;
    msg.header.stamp = ros::Time::now();
    msg.info = map_info;
    msg.data.resize(msg.info.height*msg.info.width);

    // iterate over map, store in data
    if( potential == 0.0f ) {
        std::vector<signed char>::iterator mapDataIter = msg.data.begin();
        // (0,0) is lower left corner of OccupancyGrid
        for(unsigned int j = 0; j < map_info.height; ++j ) {
            for(unsigned int i = 0; i < map_info.width; ++i ) {
                uchar val = map.at<uchar>(i,j);
                if( val == FREE ) {
                    *mapDataIter = 0;
                } else {
                    if (val < STEPABLE) {
                        *mapDataIter = 100 + val - OCCUPIED;
                    }
                    else {
                        *mapDataIter = 50 + val - STEPABLE;
                    }
                }
                ++mapDataIter;
            }
        }
    }
    else {
        std::vector<signed char>::iterator mapDataIter = msg.data.begin();
        for(unsigned int j = 0; j < map_info.height; ++j ) {
            for(unsigned int i = 0; i < map_info.width; ++i ) {
                if (map.at<uchar>(i,j) == OCCUPIED)
                    *mapDataIter = 254;
                else {
                    float c = 254.0f * std::exp( -dist_map_obst.at<float>(i,j) / 0.04 );
                    *mapDataIter = (uchar)c;
                }
                ++mapDataIter;
            }
        }
    }
    return msg;
}

void GridMap2D::setMap(const cv::Mat& binaryMap) {
    map = binaryMap.clone();
    dist_map_obst = cv::Mat(map.size(), CV_32FC1);

    cv::distanceTransform(map, dist_map_obst, CV_DIST_L2, CV_DIST_MASK_PRECISE);
    // distance map now contains distance in meters:
    dist_map_obst = dist_map_obst * map_info.resolution;
}

void GridMap2D::inflateMap(double infl_rad_obstacle, double infl_rad_stepable) {

    map = (map & (map < FREE)) + ((dist_map_obst > infl_rad_obstacle ) & (dist_map_step > infl_rad_stepable / 2.0 )) ;

    // recompute distance map with new binary map:
    cv::distanceTransform(map, dist_map_obst, CV_DIST_L2, CV_DIST_MASK_PRECISE);
    dist_map_obst = dist_map_obst * map_info.resolution;
}

// See costmap2D for mapToWorld / worldToMap implementations:

void GridMap2D::mapToWorld(unsigned int mx, unsigned int my, double& wx, double& wy) const {
    wx = map_info.origin.position.x + (mx+0.5) * map_info.resolution;
    wy = map_info.origin.position.y + (my+0.5) * map_info.resolution;
}

void GridMap2D::worldToMapNoBounds(double wx, double wy, unsigned int& mx, unsigned int& my) const {
    mx = (int) ((wx - map_info.origin.position.x) / map_info.resolution);
    my = (int) ((wy - map_info.origin.position.y) / map_info.resolution);
}

bool GridMap2D::worldToMap(double wx, double wy, unsigned int& mx, unsigned int& my) const {
    if(wx < map_info.origin.position.x || wy < map_info.origin.position.y)
        return false;

    mx = (int) ((wx - map_info.origin.position.x) / map_info.resolution);
    my = (int) ((wy - map_info.origin.position.y) / map_info.resolution);

    if(mx < map_info.width && my < map_info.height)
        return true;

    return false;
}

bool GridMap2D::inMapBounds(double wx, double wy) const{
    unsigned mx, my;
    return worldToMap(wx,wy,mx,my);
}

float GridMap2D::distanceMapAt(double wx, double wy) const{
    unsigned mx, my;

    if (worldToMap(wx, wy, mx, my))
        return dist_map_obst.at<float>(mx, my);
    else
        return -1.0f;
}

uchar GridMap2D::binaryMapAt(double wx, double wy) const{
    unsigned mx, my;

    if (worldToMap(wx, wy, mx, my))
        return map.at<uchar>(mx, my);
    else
        return 0;
}

float GridMap2D::distanceMapAtCell(unsigned int mx, unsigned int my) const{
    return dist_map_obst.at<float>(mx, my);
}

uchar GridMap2D::binaryMapAtCell(unsigned int mx, unsigned int my) const{
    return map.at<uchar>(mx, my);
}

uchar& GridMap2D::binaryMapAtCell(unsigned int mx, unsigned int my) {
    return map.at<uchar>(mx, my);
}

bool GridMap2D::isOccupiedAtCell(unsigned int mx, unsigned int my) const{
    return (map.at<uchar>(mx, my) < 255);
}

uchar GridMap2D::idMapAtCell(unsigned int mx, unsigned int my, int &id) const {
    uchar val = map.at<uchar>(mx, my);
    if( val < 255) {
        if( val < STEPABLE ){
            id = val - OCCUPIED;
            return OCCUPIED;
        } else {
            id = val - STEPABLE;
            return STEPABLE;
        }
    }
    return FREE;
}

uchar GridMap2D::idMapAt(double wx, double wy, int& id) const {
    unsigned mx, my;
    if (worldToMap(wx, wy, mx, my))
        return idMapAtCell(mx, my, id);
    else {
        id = -1; // outside of map
        return OCCUPIED;
    }
}

bool GridMap2D::isOccupiedAt(double wx, double wy) const{
    unsigned mx, my;
    if (worldToMap(wx, wy, mx, my))
        return isOccupiedAtCell(mx, my);
    else
        return true;
}

void GridMap2D::setMapCostAt(double wx, double wy, unsigned char cost, unsigned char id)
{
    unsigned mx, my;
    if (worldToMap(wx, wy, mx, my))
        map.at<uchar>(mx, my) = cost + id;
}

void GridMap2D::setMapCostAtCell(double wx, double wy, unsigned char cost, unsigned char id)
{
    unsigned mx, my;
    if (worldToMap(wx, wy, mx, my))
        map.at<uchar>(mx, my) = cost + id;
}

void GridMap2D::setConvexPolygonCost(const std::vector<double>& wx_vec, const std::vector<double>& wy_vec, unsigned char cost, unsigned char id)
{
	unsigned mx, my;
	std::vector< std::vector<cv::Point> > polygons;
	polygons.resize(1);
	polygons[0].reserve(wx_vec.size());

	// allow partially visibel polygons also
	for(int i = 0; i < wx_vec.size(); ++i) {
		worldToMapNoBounds(wx_vec[i], wy_vec[i], mx, my);
		polygons[0].push_back(cv::Point(mx, my));
	}
    cv::fillPoly( map, polygons, cost + id);
}

void GridMap2D::setConvexPolygonCost(const std::vector<geometry_msgs::Point>& points, unsigned char cost, unsigned char id)
{
    unsigned mx, my;
	std::vector< std::vector<cv::Point> > polygons;
	polygons.resize(1);
	polygons[0].reserve(points.size());

	// allow partially visibel polygons also
	for(int i = 0; i < points.size(); ++i) {
		worldToMapNoBounds(points[i].x, points[i].y, mx, my);
        polygons[0].push_back(cv::Point(my, mx));
	}
    cv::fillPoly( map, polygons, cost + id );
}
