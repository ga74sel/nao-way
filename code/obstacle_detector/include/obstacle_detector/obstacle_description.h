/**
 * @file obstacle_description.h
 *
 * @brief calibration of obstacles
 *
 * @ingroup obstacle_detector
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */

#ifndef OBSTACLE_DESCRIPTION_H
#define OBSTACLE_DESCRIPTION_H

#include <obstacle_msgs/obstacle.h>

namespace obstacle_detector
{

    /** write calibration into a obstacles vector
    *
    * @param obstacles std::vector contrainer to store obstacles
    */
    void load_obstacles(obstacle_msgs::Obstacles& obstacles)
    {
        // sugar box
        Eigen::Affine3d T;
        T = Eigen::Translation3d(-0.020, +0.009, -0.025);
        obstacles[5] = obstacle_msgs::Obstacle(5, 0.115, 0.172, 0.05, T );

        // keyboard
        T = Eigen::Translation3d(0.015, 0.142, -0.0205);
        obstacles[1] = obstacle_msgs::Obstacle(1, 0.54, 0.175, 0.042, T );

        // book
        T = Eigen::Translation3d(-0.06, -0.008, -0.0235);
        obstacles[2] = obstacle_msgs::Obstacle(2, 0.175, 0.245, 0.046, T );

        // book 2
        T = Eigen::Translation3d(0.00, -0.068, -0.015);
        obstacles[3] = obstacle_msgs::Obstacle(3, 0.240, 0.170, 0.029, T );

        // stick
        T = Eigen::Translation3d(0.04, 0.142, -0.0125);
        obstacles[4] = obstacle_msgs::Obstacle(4, 0.36, 0.035, 0.025, T );

        // stick 2
        T = Eigen::Translation3d(-0.132, 0.04, -0.0125);
        obstacles[6] = obstacle_msgs::Obstacle(6, 0.035, 0.43, 0.025, T );
    }
}

#endif // OBSTACLE_DESCRIPTION_H
