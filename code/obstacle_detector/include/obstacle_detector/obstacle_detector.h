/**
 * @file obstacle_detector.h
 *
 * @brief detect obstacles bases on NAOs camera images
 *
 * @ingroup obstacle_detector
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */

#ifndef OBSTACLE_DETECTOR_H_
#define OBSTACLE_DETECTOR_H_

#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include <cv.h>
#include <highgui.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include <aruco/aruco.h>
#include <aruco/cvdrawingutils.h>

#include <obstacle_msgs/obstacle.h>
#include <obstacle_msgs/obstacleposes.h>
#include <obstacle_msgs/detect.h>
#include <obstacle_msgs/changed.h>
#include <obstacle_msgs/clear.h>

namespace obstacle_detector
{

/** store affine transformation for a given id
*/
typedef std::map<int, std::map<int, Eigen::Affine3d>> TransformationMap;

/**
 * Implementation of an obstacle detection based on opencv and aruco lib
 *
 * Detection of obstacles by extracting aruco marker poses form nao camers.
 * The obstacle position is obtained by combining the aruco marker pose and
 * the obstacle descripition specified in obstacle_description.h
 *
 */
class ObstacleDetector
{
public:
    /** Used cameras
    */
    enum State { BOTTOM, TOP, BOTH };

    /** 
    * @brief constructor
    *
    * @param freq node frequency
    * @param display visualize detected markers in image
    * @param continous apply aruco detection to each frame
    * @param state which cameras to use
    * @param nh ros nodehandle
    */
    ObstacleDetector(double freq = 50.0, bool display = false, bool continous = false, State state = BOTH, const ros::NodeHandle& nh = ros::NodeHandle());
    virtual ~ObstacleDetector();

    /**
    * @brief run the obstacle detection (blocking)
    */
    bool run();

private:
    /**
    * @brief initalize all parameters
    */
    bool init();

    /**
    * @brief called after run function ends
    */
    bool stop();

    /**
    * @brief compute new obstacle poses
    */
    void update_pose(bool now = false);

    /**
    * @brief publish obstacle_msgs topic
    */
    void update_publisher();

    /**
    * @brief visualize images and draw markers
    */
    void update_images();

    /**
    * @brief extract marker pose wrt given frame
    * @param img image to analyse
    * @param frame frame wrt pose is computed to
    * @param draw draw into image
    */
    int extract_marker_pose(cv::Mat& img, const std::string& frame, bool draw = false);

    /**
    * @brief copy currently detected obstacles to stabel obstacle list
    * @param obstacle_det currently detected obstacles
    * @param obstacles_stable container of all ever detected obstales
    */
    void stable_obstacles(const obstacle_msgs::Obstacles& obstacle_det, obstacle_msgs::Obstacles& obstacles_stable);

    /**
    * @brief broadcast obstacles tfs over tf tree
    * @param obstacle_det currently detected obstacles
    */
    void broadcast_tf_tree(const obstacle_msgs::Obstacles& obstacle_det);

    /**
    * @brief assemble obstacle_msgs and publish
    * @param pub ros publisher to use
    * @param obstacle_det container of obstales
    * @param obstacle_ref container that describes the reference dimension of obstacles
    */
    void publish_obstalces(ros::Publisher pub, const obstacle_msgs::Obstacles& obstacle_det, const obstacle_msgs::Obstacles& obstacle_ref);

    /**
    * @brief copy currently detected obstacles to stabel obstacle list
    * @param obstacle_det currently detected obstacles
    * @param obstacles_stable container of all ever detected obstales
    */
    void publish_obstalces_changes(const std::vector<int>& enter, const std::vector<int>& leave);

    /// @brief topic callback img top
    void image_top_callback(const sensor_msgs::ImageConstPtr& msg);
    /// @brief topic callback img bottom
    void image_bottom_callback(const sensor_msgs::ImageConstPtr& msg);


    /// @brief service handle to detect obstacles
    bool detect_handler(obstacle_msgs::detect::Request& req, obstacle_msgs::detect::Response& res);
    /// @brief service handle to clear obstacles
    bool clear_handler(obstacle_msgs::clear::Request& req, obstacle_msgs::clear::Response& res);

private:
    ros::NodeHandle nh;
    ros::Publisher obstacle_pub;                // current visible obstacles
    ros::Publisher obstacle_static_pub;         // last visible obstacles ( update only via topic )
    ros::Publisher changed_pub;                 // publish new ids that have entered the field of view
    ros::ServiceServer detect_srv;              // force a detection by service
    ros::ServiceServer clear_srv;

    // transformations
    tf::TransformBroadcaster br;
    tf::TransformListener listener;

    obstacle_msgs::Obstacles obstacles_det;     // Obstacle map of detected obstacles
    obstacle_msgs::Obstacles obstacles_stable;  // stable obstalces
    obstacle_msgs::Obstacles obstacles_static;  // stable obstalces
    obstacle_msgs::Obstacles obstacles_ref;     // Obstacle map with reference information about obstaces
    TransformationMap transf_map;               // save relative transformation between visable obstacles
    std::vector<int> leave, enter;              // ids of obstacles that have entered the field of view

    // camera / images
    image_transport::ImageTransport image_transport;
    image_transport::Subscriber image_bottom_sub, image_top_sub;
    cv::Mat img_bottom, img_top;
    bool is_img_bottom_updated, is_img_top_updated;

    // aruco
    aruco::CameraParameters camera_parameters;
    aruco::MarkerDetector aruco_detector;
    float marker_size;

    std::string base_frame, cam_top_frame, cam_bottom_frame;

    State state;

    float freq;
    bool running, display, continous;
    int cnt;
};

}

#endif


