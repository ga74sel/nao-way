#include "obstacle_detector/obstacle_detector.h"

//------
// Main
//------

int main(int argc, char** argv)
{
    ros::init(argc, argv, "obstacle_detector");
    ROS_INFO_STREAM("running obstacle_detector_node...");

    // dispaly images
    // only update on srv call
    obstacle_detector::ObstacleDetector detector(30.0f, true, true, obstacle_detector::ObstacleDetector::BOTH);
    detector.run();
}
