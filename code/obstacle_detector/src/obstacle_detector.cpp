#include "obstacle_detector/obstacle_detector.h"
#include "obstacle_detector/obstacle_description.h"

#include "obstacle_msgs/math.h"

#include <iostream>

const Eigen::Vector3d zAxis = (Eigen::Vector3d() << 0, 0, 1).finished();
const Eigen::Matrix3d Rz = (Eigen::Matrix3d() << 1, 0, 0, 0, 1, 0, 0, 0, 1 ).finished();
const Eigen::Matrix3d Ry = (Eigen::Matrix3d() << 1, 0, 0, 0, 0, -1, 0, 1, 0 ).finished();
const Eigen::Matrix3d Rx = (Eigen::Matrix3d() << 0, 0, -1, 0, 1, 0, 1, 0, 0).finished();
const Eigen::Matrix3d RArr[3] = { Rx, Ry, Rz };

namespace enc = sensor_msgs::image_encodings;
using namespace obstacle_detector;

ObstacleDetector::ObstacleDetector(double freq, bool display, bool continous, State state, const ros::NodeHandle& nh)
    : nh(nh),
      image_transport(nh),
      freq(freq),
      display(display),
      continous(continous),
      state(state),
      running(false),
      is_img_top_updated(state == BOTTOM),
      is_img_bottom_updated(state == TOP),
      base_frame("/odom"),
      cam_top_frame("/CameraTop_optical_frame"),
      cam_bottom_frame("/CameraBottom_optical_frame"),
      cnt(0)
{
}

ObstacleDetector::~ObstacleDetector()
{
}

bool ObstacleDetector::run()
{
    // initalize
    if( !init() ) {
        ROS_ERROR_STREAM("ObstacleDetector::init : error");
    }

    // run main loop
    ros::Rate loop_rate(freq);
    while (ros::ok()) {
        if( continous )
            update_pose();
        if( display )
            update_images();
        update_publisher();

        ros::spinOnce();
        loop_rate.sleep();
    }

    // stop
    stop();
    return true;
}

bool ObstacleDetector::init()
{
    // subscribe to cameras
    if( state == TOP )
        image_top_sub = image_transport.subscribe("/nao_robot/camera/top/camera/image_raw", 1, &ObstacleDetector::image_top_callback, this);
    else if( state == BOTTOM)
        image_bottom_sub = image_transport.subscribe("/nao_robot/camera/bottom/camera/image_raw", 1, &ObstacleDetector::image_bottom_callback, this);
    else {
        image_top_sub = image_transport.subscribe("/nao_robot/camera/top/camera/image_raw", 1, &ObstacleDetector::image_top_callback, this);
        image_bottom_sub = image_transport.subscribe("/nao_robot/camera/bottom/camera/image_raw", 1, &ObstacleDetector::image_bottom_callback, this);
    }
    obstacle_pub = nh.advertise<obstacle_msgs::obstacleposes>("/obstacledetector/obstacleposes", 1);
    obstacle_static_pub = nh.advertise<obstacle_msgs::obstacleposes>("/obstacledetector/obstacleposes_static", 1);
    changed_pub = nh.advertise<obstacle_msgs::changed>("/obstacledetector/changed", 1);

    // services
    detect_srv = nh.advertiseService("/obstacledetector/detect", &ObstacleDetector::detect_handler, this);
    clear_srv = nh.advertiseService("/obstacledetector/clear", &ObstacleDetector::clear_handler, this);

    // camera parameter
    cv::Mat dist = (cv::Mat_<double>(4, 1) << -0.056, 0.04, 0.0009, -0.021);
    cv::Mat cameraK = (cv::Mat_<double>(3, 3) << 568.816378, 0.000000, 282.989222, 0.000000, 559.649417, 189.409149, 0.000000, 0.000000, 1.000000);
    camera_parameters.setParams(cameraK,dist,cv::Size(640,480));    
    marker_size = 0.066f;

    // load some test obstacles
    load_obstacles(obstacles_ref);
    return true;
}

bool ObstacleDetector::stop()
{
}

void ObstacleDetector::update_pose(bool now)
{
    if(cnt == 10  || now) {
        if( is_img_bottom_updated && is_img_top_updated)
        {
            // 1. extract currently visable obstacles from images
            obstacles_det.clear();
            if( state == TOP )
                extract_marker_pose(img_top, cam_top_frame, display);
            else if( state == BOTTOM )
                extract_marker_pose(img_bottom, cam_bottom_frame, display);
            else {
                //extract_marker_pose(img_top, cam_top_frame, display);
                extract_marker_pose(img_bottom, cam_bottom_frame, display);
            }

            // 2. detect obstacles that leave or enter the field of view
            leave.clear();
            enter.clear();
            stable_obstacles(obstacles_det, obstacles_stable);
        }
    }
    cnt++;
    cnt = cnt > 10 ? 0 : cnt;
}

void ObstacleDetector::update_images()
{
    if( is_img_bottom_updated && is_img_top_updated)
    {
        cv::waitKey(1);
        if( state == TOP )
            cv::imshow("top", img_top);
        else if( state == BOTTOM )
            cv::imshow("bottom", img_bottom);
        else {
            cv::imshow("top", img_top);
            cv::imshow("bottom", img_bottom);
        }
    }
}

void ObstacleDetector::update_publisher()
{
    if( is_img_bottom_updated && is_img_top_updated) {
        // 3. publish information
        broadcast_tf_tree(obstacles_stable);
        publish_obstalces(obstacle_pub, obstacles_stable, obstacles_ref);
        publish_obstalces(obstacle_static_pub, obstacles_static, obstacles_ref);
        publish_obstalces_changes(enter, leave);
    }
}

int ObstacleDetector::extract_marker_pose(cv::Mat& img, const std::string& frame, bool draw)
{
    Eigen::Affine3d TM_C, TC_0;
    double ypr[3];

    // get transformation to base frame
    tf::StampedTransform transform;
    try {
        listener.lookupTransform(base_frame, frame, ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("ObstacleDetector::extract_marker_pose: no transformation");
        return 0;
    }
    tf::transformTFToEigen(transform, TC_0);

    // detect markers
    std::vector<aruco::Marker> aruco_markers;
    aruco_detector.detect(img, aruco_markers);

    // save markers in map
    for( size_t i = 0; i < aruco_markers.size(); ++i )
    {
        // convert to eigen transformation matrix
        aruco_markers[i].calculateExtrinsics(marker_size, camera_parameters, false);
        math::cvpose2eigen(aruco_markers[i].Tvec, aruco_markers[i].Rvec, TM_C);

        // change trafo:
        Eigen::Affine3d TC_sensor;
        TC_sensor= Eigen::AngleAxisd(0.20, Eigen::Vector3d::UnitX() ), 0, 0, 0, 1;

        // create the observed obstacle
        int id = aruco_markers[i].id;

        auto it = obstacles_ref.find( id );
        if( it != obstacles_ref.end() ) {
            // save the transfromations
            obstacles_det[id].T_raw = TC_0 * TC_sensor * TM_C;
            obstacles_det[id].T = TC_0 * TC_sensor * TM_C * obstacles_ref[id].TCP_M;
            // find the main orientation axis
            int axis;
            Eigen::Vector3d zCP;
            zCP = obstacles_det[id].T.rotation().transpose() * zAxis;
            zCP.cwiseAbs().maxCoeff(&axis);
            // find the rotation angle
            obstacles_det[id].main_axis = axis;
            obstacles_det[id].id = id;
            math::eigenGetEulerYPR(obstacles_det[id].T.rotation() * RArr[axis].transpose(), ypr[2], ypr[1], ypr[0], true);
            obstacles_det[id].theta = ypr[2];
        }

        if( draw ) {
             aruco_markers[i].draw(img, cv::Scalar(0,0,255), 2);
             aruco::CvDrawingUtils::draw3dAxis(img, aruco_markers[i], camera_parameters);
        }
    }
    return aruco_markers.size();
}

void ObstacleDetector::stable_obstacles(const obstacle_msgs::Obstacles& obstacles_det, obstacle_msgs::Obstacles& obstacles_stable)
{
    double ypr[3];
    ros::Time cur_time = ros::Time::now();

    // iterate over detected obstacles_det
    for( auto it = obstacles_det.begin(); it != obstacles_det.end(); ++it ) {
        // find and update same obstacle in obstacles_stable
        int id = it->first;
        auto it_find = obstacles_stable.find( id );
        if( it_find == obstacles_stable.end() ) {
            // obstacle is new save it in stablel list, remember time
            enter.push_back( id );
        }
        obstacles_stable[id] = it->second;
        obstacles_stable[id].time = cur_time;

        // update the transformation map that contains relative transformations between visable obstacles
        auto jt = it; ++jt;
        for( ; jt != obstacles_det.end(); ++jt ) {
            int jd = jt->first;
            auto& Ti_0 = it->second.T;
            auto& Tj_0 = jt->second.T;
            transf_map[jd][id] = Tj_0.inverse() * Ti_0; // Ti_j, i wrt j
            transf_map[id][jd] = Ti_0.inverse() * Tj_0; // Tj_i, j wrt i
        }
    }
}

void ObstacleDetector::broadcast_tf_tree(const obstacle_msgs::Obstacles& obstacles_det)
{
    std::vector<tf::StampedTransform> tf_transformations;
    tf::Transform tf_transform;
    std::string child_id = "aruco_";

    int i = 0;
    tf_transformations.resize(obstacles_det.size() * 2);
    for( auto it = obstacles_det.begin(); it != obstacles_det.end(); ++it ) {
        tf::transformEigenToTF( it->second.T_raw , tf_transform );
        tf_transformations[i].setData( tf_transform );
        tf_transformations[i].stamp_ = ros::Time::now();
        tf_transformations[i].frame_id_ = cam_bottom_frame;
        tf_transformations[i].child_frame_id_ = child_id + std::to_string(it->first);

        tf::transformEigenToTF( it->second.T , tf_transform );
        tf_transformations[i+1].setData( tf_transform );
        tf_transformations[i+1].stamp_ = ros::Time::now();
        tf_transformations[i+1].frame_id_ = base_frame;
        tf_transformations[i+1].child_frame_id_ = child_id + std::to_string(it->first) + "_odom";

        i+=2;
    }
    br.sendTransform(tf_transformations);
}

void ObstacleDetector::publish_obstalces(ros::Publisher pub, const obstacle_msgs::Obstacles& obstacles_det, const obstacle_msgs::Obstacles& obstacles_ref)
{
    obstacle_msgs::obstacleposes msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = base_frame;
    auto& msg_obstacles = msg.obstacleposes;
    msg_obstacles.resize( obstacles_det.size() );
    int i = 0;
    for( auto it = obstacles_det.begin(); it != obstacles_det.end(); ++it )
    {
        auto& obstacle_det = it->second;
        auto& obstacle_ref = obstacles_ref.at(obstacle_det.id);
        auto& obstalce_msg = msg_obstacles[i];

        obstalce_msg.id.data = obstacle_det.id;
        obstalce_msg.main_axis.data = obstacle_det.main_axis;
        obstalce_msg.footprint = obstacle_ref.footprints[obstacle_det.main_axis];
        obstalce_msg.height.data = obstacle_ref.height[obstacle_det.main_axis];
        obstalce_msg.pose.theta = obstacle_det.theta;
        obstalce_msg.pose.x = obstacle_det.T.translation()(0);
        obstalce_msg.pose.y = obstacle_det.T.translation()(1);
        obstalce_msg.marker.x = obstacle_det.T_raw.translation()(0);
        obstalce_msg.marker.y = obstacle_det.T_raw.translation()(1);
        obstalce_msg.marker.z = obstacle_det.T_raw.translation()(2);
        i++;
    }
    pub.publish(msg);
}

void ObstacleDetector::publish_obstalces_changes(const std::vector<int>& enter, const std::vector<int>& leave)
{
    if( enter.empty() && leave.empty() )
        return;

    obstacle_msgs::changed msg;
    msg.enter.resize(enter.size());
    for( size_t i = 0; i < enter.size(); ++i )
        msg.enter[i].data = enter[i];
    msg.leave.resize(leave.size());
    for( size_t i = 0; i < leave.size(); ++i )
        msg.leave[i].data = leave[i];
    changed_pub.publish(msg);
}

void ObstacleDetector::image_top_callback(const sensor_msgs::ImageConstPtr& msg)
{
    cv_bridge::CvImageConstPtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvShare(msg, enc::BGR8);
        img_top = cv_ptr->image.clone();
        is_img_top_updated = true;
    }
    catch (cv_bridge::Exception& except) {
        ROS_ERROR("ObstacleDetector::image_top_sub: %s", except.what());
    }
}

void ObstacleDetector::image_bottom_callback(const sensor_msgs::ImageConstPtr& msg)
{
    cv_bridge::CvImageConstPtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvShare(msg, enc::BGR8);
        img_bottom = cv_ptr->image.clone();
        is_img_bottom_updated = true;
    }
    catch (cv_bridge::Exception& except) {
        ROS_ERROR("ObstacleDetector::image_bottom_sub: %s", except.what());
    }
}

bool ObstacleDetector::detect_handler(obstacle_msgs::detect::Request& req, obstacle_msgs::detect::Response& res)
{
    if( is_img_bottom_updated && is_img_top_updated)
    {
        // detect and save obstacles
        update_pose(true);
        obstacles_static = obstacles_stable;

        ROS_INFO_STREAM("ObstacleDetector::detect_handler static size: " << obstacles_static.size());
        ROS_INFO_STREAM("ObstacleDetector::detect_handler cur size: " << obstacles_det.size());

        // return detected obstacles
        res.header.stamp = ros::Time::now();
        res.header.frame_id = base_frame;
        auto& msg_obstacles = res.obstacleposes;
        msg_obstacles.resize( obstacles_static.size() );
        int i = 0;
        for( auto it = obstacles_static.begin(); it != obstacles_static.end(); ++it ) {
            auto& obstacle_det = it->second;
            auto& obstacle_ref = obstacles_ref.at(obstacle_det.id);
            auto& obstalce_msg = msg_obstacles[i];

            obstalce_msg.id.data = obstacle_det.id;
            obstalce_msg.main_axis.data = obstacle_det.main_axis;
            obstalce_msg.footprint = obstacle_ref.footprints[obstacle_det.main_axis];
            obstalce_msg.height.data = obstacle_ref.height[obstacle_det.main_axis];
            obstalce_msg.pose.theta = obstacle_det.theta;
            obstalce_msg.pose.x = obstacle_det.T.translation()(0);
            obstalce_msg.pose.y = obstacle_det.T.translation()(1);
            obstalce_msg.marker.x = obstacle_det.T_raw.translation()(0);
            obstalce_msg.marker.y = obstacle_det.T_raw.translation()(1);
            obstalce_msg.marker.z = obstacle_det.T_raw.translation()(2);
            i++;
        }
        return true;
    }
    return false;
}

bool ObstacleDetector::clear_handler(obstacle_msgs::clear::Request& req, obstacle_msgs::clear::Response& res)
{
    if( req.ids.empty() || req.ids[0] == -1 ) {
        ROS_INFO_STREAM("clear all obstacles");
        transf_map.clear();
        obstacles_stable.clear();
        obstacles_static.clear();
    }
    else {
        for(size_t i = 0; i < req.ids.size(); ++i) {
            auto it = obstacles_stable.find(req.ids[i]);
            if( it != obstacles_stable.end() ) {
                obstacles_stable.erase(it);
            }

            auto jt = obstacles_static.find(req.ids[i]);
            if( jt != obstacles_static.end() )
                obstacles_static.erase(jt);
        }
    }
    return true;
}

