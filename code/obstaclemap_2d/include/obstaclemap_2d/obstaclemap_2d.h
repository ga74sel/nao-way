/**
 * @file utilities.h
 *
 * @brief generate a 2d occupancy grid based on detected obstacles
 *        form obstacle detector package
 *
 * @ingroup obstaclemap_2d
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */

#include <gridmap_2d/gridmap_2d.h>
#include <ros/ros.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Polygon.h>
#include <nav_msgs/OccupancyGrid.h>
#include <obstacle_msgs/obstacleposes.h>
#include <obstacle_msgs/obstacle.h>
#include <obstacle_msgs/clear.h>

#include <Eigen/Dense>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

namespace obstaclemap_2d
{

/**
 * Store information about obstacle footprint
 *
 */
class Footprint
{
public:
    std::vector<geometry_msgs::Point> footprint;
    float height;
    int id;
};
typedef std::vector<Footprint> Footprints;


/**
 * Implementation of a 2d grid map based
 * Generates a nav_msgs/OccupancyGrid Message based on the detected
 * obstacles from obstacle_detector package
 *
 * Internal representation of map is a cv::mat object,
 * maipulations of this array are handeld by gridmap_2d::GridMap2D package
 *
 */
class ObstacleMap2D
{
public:

    /**
    * @brief constructor
    *
    * @param freq node frequency
    * @param nh ros nodehandle
    */
    ObstacleMap2D(double freq = 50.0, const ros::NodeHandle& nh = ros::NodeHandle());
    virtual ~ObstacleMap2D();

    /**
    * @brief run the obstacle detection (blocking)
    */
    bool run();

private:
    /**
    * @brief initalize all parameters
    */
    bool init();

    /**
    * @brief compute the internal map
    */
    void update();

    /**
    * @brief clear old map and insert new obstacles
    */
    void rebuild_map();

    /**
    * @brief convert mat array to nav msg
    */
    void publish_map();

    /// @brief topic callback of detected obstacles
    void obstacle_callback(obstacle_msgs::obstacleposesConstPtr msg);

    /// @brief service handle to clear map
    bool clear_map_handler(obstacle_msgs::clearRequest& req, obstacle_msgs::clearResponse& res);

private:
    ros::NodeHandle nh;
    gridmap_2d::GridMap2DPtr grid_map;      // ptr to internal grid_map
    ros::Subscriber obstacle_sub;           // sub to detected obstacles
    ros::Publisher map_pub;                 // publish map

    std::string map_frame;                  // frame

    Footprints footprints;    // generated footprints
    bool is_map_updated;                    
    float rate;
    float infl_rad_obstacle, infl_rad_stepable;
    float step_height;

    // transformations
    tf::TransformListener listener;

};

}
