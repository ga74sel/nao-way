/**
 * @file utilities.h
 *
 * @brief helper function to convert geometry_msgs::polygon
 *
 * @ingroup obstaclemap_2d
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */

#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <Eigen/Geometry>

namespace mm
{

/**
* @brief transforms a geometry_msgs::polygon by a given geometry_msgs::pose2D
* @param polygon input polygon
* @param pose2D input transformation
* @return ouput polygon
*/
inline geometry_msgs::Polygon transform_polygon(const geometry_msgs::Polygon& polygon, const geometry_msgs::Pose2D& pose2D) {
        geometry_msgs::Polygon polygon_transf;
        polygon_transf.points.resize(polygon.points.size());

        float c = cos(pose2D.theta);
        float s = sin(pose2D.theta);

        for( size_t i = 0; i < polygon_transf.points.size(); ++i) {
                polygon_transf.points[i].x = c*polygon.points[i].x - s*polygon.points[i].y + pose2D.x;
                polygon_transf.points[i].y = c*polygon.points[i].y + s*polygon.points[i].x + pose2D.y;
                polygon_transf.points[i].z = polygon.points[i].y;
        }
        return polygon_transf;
}

/**
* @brief transforms a geometry_msgs::polygon by a given Eigen Transformation
* @param polygon input polygon
* @param T eigen transformation
* @return ouput polygon
*/
inline geometry_msgs::Polygon transform_polygon(const geometry_msgs::Polygon& polygon, const Eigen::Affine3d& T) {
        geometry_msgs::Polygon polygon_transf;
        polygon_transf.points.resize(polygon.points.size());

        // to eigen
        Eigen::MatrixXd Pts(4, polygon.points.size());
        for( int i = 0; i < polygon.points.size(); ++i) {
            Pts.col(i) << polygon.points[i].x, polygon.points[i].y, 0.0, 1.0;
        }

        // transform
        Eigen::MatrixXd Pts_transf = T.matrix() * Pts;

        // to geomety_msgs::Polygon
        for( int i = 0; i < polygon.points.size(); ++i) {
            polygon_transf.points[i].x = Pts_transf.col(i)[0];
            polygon_transf.points[i].y = Pts_transf.col(i)[1];
        }
        return polygon_transf;
}

/**
* @brief convert geometry_msgs::polygon to a point vector
* @param polygon input polygon
* @return ouput Point vector
*/
inline std::vector<geometry_msgs::Point> polygon2points(const geometry_msgs::Polygon& polygon) {
        std::vector<geometry_msgs::Point> points;
        points.resize(polygon.points.size());

        for( size_t i = 0; i < polygon.points.size(); ++i) {
                points[i].x = polygon.points[i].x;
                points[i].y = polygon.points[i].y;
                points[i].z = polygon.points[i].z;
        }
        return points;
}
};
