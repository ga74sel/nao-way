#include "obstaclemap_2d/obstaclemap_2d.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "obstacle_map");
    ROS_INFO_STREAM("obstacle_map...");

    obstaclemap_2d::ObstacleMap2D map;
    map.run();

    return 0;
}