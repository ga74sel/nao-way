#include "obstaclemap_2d/obstaclemap_2d.h"
#include "obstaclemap_2d/utilities.h"

const Eigen::Vector3d zAxis = (Eigen::Vector3d() << 0, 0, 1).finished();

using namespace obstaclemap_2d;

ObstacleMap2D::ObstacleMap2D(double freq, const ros::NodeHandle& nh)
    : nh(nh),
      rate(freq),
      is_map_updated(true)
{
}

ObstacleMap2D::~ObstacleMap2D()
{
}

bool ObstacleMap2D::init()
{
    map_pub = nh.advertise<nav_msgs::OccupancyGrid>("map", 1, true);
    obstacle_sub = nh.subscribe("/obstacledetector/obstacleposes_static", 1, &ObstacleMap2D::obstacle_callback, this);

    // load map parametes
    bool ret = true;
    float width, height, resolution;

    infl_rad_obstacle = 0.22;
    infl_rad_stepable = 0.18;

    width = 3.5f;
    height = 3.5f;
    resolution = 0.005f;
    step_height = 0.046;
    map_frame = "/base_footprint";

    // construct map
    if( ret ) {
        grid_map = std::make_shared<gridmap_2d::GridMap2D>(width, height, resolution, map_frame);
        ROS_INFO_STREAM("Create new map: width= " << grid_map->size().width << "px height=" << grid_map->size().height << "px");
        ROS_INFO_STREAM("Resolution=" << grid_map->getResolution());
    }
    return ret;
}

bool ObstacleMap2D::run()
{
    if(!init())
        return false;

    ros::Rate loop_rate(rate);
    while(ros::ok()) {
        update();
        ros::spinOnce();
        loop_rate.sleep();
    }
}

void ObstacleMap2D::update()
{
    if( is_map_updated ) {
        is_map_updated = false;
        rebuild_map();
        publish_map();
    }
}

void ObstacleMap2D::rebuild_map()
{
    // clear old map information
    grid_map->clearMap();

    // insert into map
    // change costs depending on step height
    for( int i = 0; i < footprints.size(); ++i) {
        if( footprints[i].height <= step_height ) {
            grid_map->setConvexPolygonCost( footprints[i].footprint, gridmap_2d::GridMap2D::STEPABLE, footprints[i].id );
        }
        else
            grid_map->setConvexPolygonCost( footprints[i].footprint, gridmap_2d::GridMap2D::OCCUPIED, footprints[i].id );
    }
    grid_map->updateDistanceMap();

    // inflate map
    grid_map->inflateMap(infl_rad_obstacle, infl_rad_stepable);
}

void ObstacleMap2D::publish_map()
{
    map_pub.publish( grid_map->toOccupancyGridMsg() );
}

void ObstacleMap2D::obstacle_callback(obstacle_msgs::obstacleposesConstPtr msg)
{
    is_map_updated = true;
    auto& obstacleposes = msg->obstacleposes;
    std::string obstacle_frame = msg->header.frame_id;

    // get the transformation into map frame
    Eigen::Affine3d TO_M, Tobst, T;

    // get transformation obstacle_frame wrt map_frame
    tf::StampedTransform transform;
    listener.waitForTransform(map_frame, obstacle_frame, ros::Time(0), ros ::Duration(0.2));
    try {
        listener.lookupTransform(map_frame, obstacle_frame, ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("ObstacleDetector::extract_marker_pose: no transformation");
        return;
    }
    tf::transformTFToEigen(transform, TO_M);

    // transform every obstacle footprint
    footprints.resize( obstacleposes.size() );
    for( int i = 0; i < obstacleposes.size(); ++i) {
        // transform into map_frame
        auto& pose = obstacleposes[i].pose;
        Eigen::Vector3d position;
        position << pose.x, pose.y, 0.0;
        Tobst = Eigen::Translation3d(position) * Eigen::AngleAxisd(pose.theta,zAxis);
        T = TO_M * Tobst;

        // transform footprint based on objects pose
        geometry_msgs::Polygon polygon;
        polygon = mm::transform_polygon(obstacleposes[i].footprint, T);

        footprints[i].footprint = mm::polygon2points(polygon);
        footprints[i].height = obstacleposes[i].height.data;
        footprints[i].id = obstacleposes[i].id.data;
    }
}
