#include "planner/planner.h"

#define TOL 0.02
#define MAX_ITER 30
#define MAX_ANGL M_PI / 2.0

using namespace planner;
using namespace gridmap_2d;

Planner::Planner(const std::string& name, const ros::NodeHandle& nh)
    : PlannerBase(name, nh),
      d_step(0.1),
      dx(0.01),
      dth(0.04363222),
      d_safe(0.1),
      d_approach(0.22),
      d_turn(0.10),
      stepheight(0.027)
{
}

Planner::~Planner()
{
}

/* initalize planner */
bool Planner::init(ros::NodeHandle& nh)
{

    bool ret = true;
    obstacle_sub = nh.subscribe("/obstacledetector/obstacleposes_static", 1, &Planner::obstacle_callback, this);
    return ret;
}

/* map has changed */
void Planner::map_changed(gridmap_2d::GridMap2DPtr map)
{
}

/* create a new plan */
bool Planner::make_plan(const geometry_msgs::Pose2D& start_pose,
                        const geometry_msgs::Pose2D& goal_pose,
                        const gridmap_2d::GridMap2DPtr& map,
                        PathStatePath& state_path)
{
    PathState start, goal;
    std::vector<PathState> interm;

    // compute map size
    cv::Point2f map_size_cv = 0.5 * map->worldSize();
    Eigen::Vector2f map_size;
    map_size << map_size_cv.x, map_size_cv.y;

    // convert to eigen pose
    start.p << start_pose.x, start_pose.y;
    start.th = start_pose.theta;
    goal.p << goal_pose.x, goal_pose.y;

    // insert first state
    state_path.push_back(start);

    // check if goal point is reachable
    if( map->inMapBounds(goal_pose.x, goal_pose.y) ) {
        if( map->isOccupiedAt(goal_pose.x, goal_pose.y) ) {
            ROS_ERROR_STREAM("GOAL occupied / not part ");
            return false;
        }
    }
    else {
        compute_map_goal(map_size, start, goal);
    }

    // create path
    int cnt = 0;
    while( !plan_intermediate(map, start, goal, interm ) ) {

        // save intermeditte goals
        std::copy(interm.begin(), interm.end(), std::back_inserter(state_path));

        // update path
        start = interm.back();
        interm.clear();
        cnt++;

        // check for errors
        if( !map->inMapBounds(start.p[0], start.p[1]) ) {
            ROS_INFO_STREAM("interm goal outside of map");
            break;
        }
        if( cnt > MAX_ITER ) {
            ROS_INFO_STREAM("MAX_ITER limit hit");
            break;
        }
    }

    //set last point as goal
    goal.th = std::atan2(goal.p[1] - state_path.back().p[1], goal.p[0] - state_path.back().p[0]);
    state_path.push_back(goal);
    return true;
}

bool Planner::plan_intermediate(const gridmap_2d::GridMap2DPtr& map,
                                const PathState& s_,
                                const PathState& g,
                                std::vector<PathState>& g_i)
{
    Eigen::Vector2f n;  // direction normal vector, right left
    PathState s = s_;

    RayTrace ray;       // save information of raytracing result

    float th;           // current theta
    float th_goal;      // theta to goal point

    // distance to goal
    n = g.p - s.p;

    // reltative angle to goal
    th_goal = std::atan2(n[1], n[0]);
    th = -rotation_angle(s.th, th_goal);
    s.th += th;

    // check the direct connection
    ray_tracing_single(map, s.p, s.th, ray);

    // check direct connection
    if( ray.type == GridMap2D::FREE ) {             // obstacle found
        ROS_INFO_STREAM("->PATH is clear");
        g_i.push_back( PathState(ray.x_vec[0], s.th) );
    }
    else if( ray.type == GridMap2D::OCCUPIED ) {    // path blocked
        ROS_INFO_STREAM("->PATH move arround");
        plan_around_corner(map, s, dth, g_i);
    }
    else if( ray.type == GridMap2D::STEPABLE ) {
        ROS_INFO_STREAM("->PATH approach obstacle");
        if( !plan_center_point(map, s, ray.id, g_i) ) {
            plan_around_corner(map, s, dth, g_i);
        }
    }
    return (g_i.back().p).norm() < TOL;
}

bool Planner::plan_around_corner(const gridmap_2d::GridMap2DPtr& map,
                                 const PathState& s,
                                 float dth,
                                 std::vector<PathState>& x)
{
    std::vector<PathState> x_r, x_l;
    bool success_r, success_l;

    // check path to the right
    success_r = plan_single_point(map, s, dth, x_r);

    // check path to the left
    success_l = plan_single_point(map, s, -dth, x_l);

    // select the better of the two
    if( success_r & ~success_l) {
        std::copy (x_r.begin(), x_r.end(), std::back_inserter(x));
    }
    else if( ~success_r & success_l) {
        std::copy (x_l.begin(), x_l.end(), std::back_inserter(x));
    }
    else if( success_r & success_l ) {
        // select the path that leads to less roation
        if( std::abs(s.th - x_r[0].th) < std::abs(s.th - x_l[0].th) ) {
            std::copy (x_r.begin(), x_r.end(), std::back_inserter(x));
        }
        else {
            std::copy (x_l.begin(), x_l.end(), std::back_inserter(x));
        }
    }
    else {
        ROS_INFO_STREAM("plan_around_corner fail - return false");
        return false;
    }
    return true;
}

bool Planner::plan_single_point(const gridmap_2d::GridMap2DPtr& map,
                                const PathState& s,
                                float dth,
                                std::vector<PathState>& x)
{
    bool free = false;
    bool success = true;
    float prev_d = 0;
    Eigen::Vector2f p;
    Eigen::Vector2f prev_p;
    RayTrace ray;
    float th;

    th = 0.0;
    p.setZero();
    prev_p.setZero();

    ray_tracing_single(map, s.p, s.th, ray);
    if( ray.x_vec.empty() )
        return false;
    prev_p = ray.x_vec[0]; prev_d = ray.d;

    /*
     * sweep through +- MAX_ANGL until a free ray was found
     * or a object corner was detected
     */
    while( !free )
    {
        // raytrace to next obstacle
        if( ray_tracing_single(map, s.p, s.th + th, ray) ) {
            x.push_back( PathState(prev_p + d_safe * ray.n, s.th + th) );
            free = true;
        }

        if( ray.x_vec.empty() )
            return false;

        // detect a jump in ray lenght = corner
        if( std::norm(ray.d - prev_d) > 2.0f * d_safe ) {
            x.push_back( PathState(prev_p + d_safe * ray.n, s.th + th) );
            free = true;
        }
        // stepable obstacle found
        if( ray.type == GridMap2D::STEPABLE ) {
            std::vector<PathState> x_vec;
            if( plan_center_point(map, s, ray.id, x_vec) ) {
                std::copy (x_vec.begin(), x_vec.end(), std::back_inserter(x));
                free = true;
            }
        }

        th += dth;
        prev_d = ray.d;
        prev_p = ray.x_vec[0];
        if( std::abs(th) > MAX_ANGL ) {
            success = false;
            //ROS_INFO_STREAM("MAX_ANGL HIT - return false");
            break;
        }
    }
    return success;
}

/* plan to next obstacle center point */
bool Planner::plan_center_point(const GridMap2DPtr& map,
                                const PathState& s,
                                int obstacle_id,
                                std::vector<PathState>& x)
{

    PathState xa;           // approach point
    PathState xt;           // turn point
    PathState xf;           // finish point

    Eigen::Vector2f n;      // direction
    float d;                // distance

    // find approch point
    if( !compute_object_approach_point(s, xa, obstacle_id) )
        return false;

    // find turn point
    n << cos(xa.th), sin(xa.th);
    xt.p = xa.p - d_turn*n;
    xt.th = std::atan2( xt.p[1]-s.p[1], xt.p[0]-s.p[0]);
    d = (s.p - xt.p).norm();

    if( d > d_approach) {
        x.push_back( xt );
    }

    // push back the step point
    x.push_back( xa );

    // point after approch/step to continue next plan
    xf.p = xa.p + xa.length * n;
    xf.th = xa.th;
    x.push_back( xf);

    return true;
}

bool Planner::compute_object_approach_point(const PathState& s, PathState& x, int id)
{
    // check if obstacle id is known
    if( obstacle_map.find(id) == obstacle_map.end() ) {
        return false;
    }

    const Eigen::Matrix<double,3,4>& P = obstacle_map[id].Pts_B;
    Eigen::Vector2d sd;
    sd = s.p.cast<double>();

    // get closest approach point
    double d_min = std::numeric_limits<double>::infinity();
    int i_min = 0;
    for(size_t i = 0; i < 4; ++i) {
        double d = (sd - P.col(i).head(2) ).norm();
        if( d < d_min ) {
            i_min = i;
            d_min = d;
            x.p = P.col(i).head(2).cast<float>();
        }
    }

    // get heigth, length
    x.height = obstacle_map[id].height;
    x.length = obstacle_map[id].length[i_min];

    // get the rotation angle
    float th;
    Eigen::Rotation2D<double> R(0);
    R.fromRotationMatrix(obstacle_map[id].TO_B.rotation().matrix());
    th = (float)R.angle();
    while( th > M_PI / 4.0 )
        th -= M_PI / 2.0;
    while( th < -M_PI / 4.0)
        th += M_PI / 2.0;
    x.th = th;

    // set path state
    x.id = id;
    if( x.height > stepheight )
        x.state = PathState::APPROACH;
    else
        x.state = PathState::STEP;
    return true;
}

void Planner::compute_map_goal(const Eigen::Vector2f& map_size, const PathState &start_pose, PathState &goal_pose )
{
    Eigen::Vector2f d = goal_pose.p - start_pose.p;
    float th = std::atan2( d[1], d[0]);

    if( th > -M_PI/4 || th < M_PI/4 ) {
        goal_pose.p[0] = map_size[0];
        goal_pose.p[1] = th * map_size[0];
    }
    else if( th < -M_PI/4 ) {
        goal_pose.p[1] = map_size[1];
        goal_pose.p[0] = map_size[1] / th;
    }
    else if( th > -M_PI/4 ) {
        goal_pose.p[1] = -map_size[1];
        goal_pose.p[0]= map_size[1] / th;
    }
    goal_pose.th = th;
}

bool Planner::ray_tracing_dist(const gridmap_2d::GridMap2DPtr& map, const Eigen::Vector2f& x_s, float th, float d)
{
    Eigen::Vector2f n, x;
    n << cos(th), sin(th);

    // check for collision
    // move from end pt to start pt
    for(float s = d; s > 0.0; s -= dx) {
        x = x_s + s * n;
        if( map->isOccupiedAt(x[0], x[1]) )
            return false;
    }
    return true;
}

bool Planner::ray_tracing_single(const gridmap_2d::GridMap2DPtr& map, const Eigen::Vector2f &x_s, float th, RayTrace &ray)
{
    bool hit = false;
    bool left = false;
    bool finished = false;
    Eigen::Vector2f x;
    int id = 0;

    ray.x_vec.clear();

    // direction
    ray.n << cos(th), sin(th);

    // check for collision
    float s = 10.0*dx;
    int cnt = 0;
    while( !finished ) {
        x = x_s + s * ray.n;
        ray.type = map->idMapAt(x[0], x[1], id);

        if( ray.type != GridMap2D::FREE ) { // non free gird detected
            if( !hit ) {
                hit = true;
                ray.x_vec.push_back(x);     // inflation ray hit
            }
            if( id != 0) {
                finished = true;
                ray.x_vec.push_back(x);     // obstacle/map border ray hit
                ray.id = id;
            }
            if(hit)
                cnt++;
        }
        if( ray.type == GridMap2D::FREE && cnt > 20) {   // again free grid detected, ray left inflation
            finished = true;
            ray.type = GridMap2D::OCCUPIED;
            left = true;
        }
        s += dx;
    }

    // something went wrong
    if( ray.x_vec.empty() )
        return false;

    // closest hit distance
    ray.d = (ray.x_vec[0] - x_s).norm();

    // ray intersected with inflation and left inflation
    if( left )
        return false;

    // hit point is not an obstacle but outside of local map
    if( !map->inMapBounds(ray.x_vec[0][0], ray.x_vec[0][1]) ) {
        return true;
    }
    return false;
}

bool Planner::ray_tracing_multi(const gridmap_2d::GridMap2DPtr& map, const Eigen::Vector2f &x_s, float th,
                        std::vector<Eigen::Vector2f>& x_vec_in,
                        std::vector<Eigen::Vector2f>& x_vec_out,
                        Eigen::Vector2f& x_f,
                        Eigen::Vector2f& n)
{
    Eigen::Vector2f x, x_hit;
    bool inside = false;
    bool occupied = false;
    bool prev_occupied = false;
    int cnt = 0;
    x_vec_in.clear();
    x_vec_out.clear();
    n << cos(th), sin(th);

    // check for collision
    float s = 10.0*dx;
    while( map->inMapBounds(x[0], x[1]) ) {

        // check new field
        x = x_s + s * n;
        occupied = map->isOccupiedAt(x[0], x[1]);

        // change along the ray
        if( occupied != prev_occupied ) {
            cnt = 0;
            x_hit = x;
        }

        if( occupied && cnt > 10 ) {
            // definitly inside object
            if( !inside ) {
                inside = true;
                x_vec_in.push_back(x_hit);
            }
        }
        else if( !occupied && cnt > 10 ) {
            // definitly outside object
            if( inside ) {
                inside = false;
                x_vec_out.push_back(x_hit);
            }
        }

        s += dx;
        prev_occupied = occupied;
        cnt++;
    }

    // true: no obstace detected
    // false: obstacle(s) along the way
    x_f = x;
    return x_vec_in.empty();
}

float Planner::rotation_angle(float src, float dst)
{
    return (src > dst) ? -M_PI + std::fmod(src - dst + M_PI, 2.0*M_PI)
                       :  M_PI - std::fmod(dst - src + M_PI, 2.0*M_PI);
}


void Planner::obstacle_callback(obstacle_msgs::obstacleposesConstPtr msg)
{
    auto& obstacleposes = msg->obstacleposes;
    std::string obstacle_frame = msg->header.frame_id;

    // transformations
    Eigen::Affine2d TO;         // 2D transformation obstacle wrt obstacle frame
    Eigen::Affine2d TO_B;       // 2D transformation obstacle wrt base frame
    Eigen::Affine2d T;
    Eigen::Affine3d TO_B_3D;    // 3D transformation obstacle wrt base frame

    // get transformation obstacle_frame wrt robot_frame
    tf::StampedTransform transform;
    listener.waitForTransform(PlannerBase::get_robot_frame(), obstacle_frame, ros::Time(0), ros ::Duration(0.2));
    try {
        listener.lookupTransform(PlannerBase::get_robot_frame(), obstacle_frame, ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("Planner::extract_marker_pose: no transformation");
        return;
    }
    tf::transformTFToEigen(transform, TO_B_3D);
    TO_B = Eigen::Translation2d(TO_B_3D.translation().head(2)) * TO_B_3D.linear().topLeftCorner(2,2);

    // transform every obstacle
    for( int i = 0; i < obstacleposes.size(); ++i) {
        int id = obstacleposes[i].id.data;
        auto& pose = obstacleposes[i].pose;

        // find the transformation obstacle wrt robot
        Eigen::Vector2d position;
        position << pose.x, pose.y;
        TO = Eigen::Translation2d(position) * Eigen::Rotation2D<double>(pose.theta);
        T = TO_B * TO;

        if( obstacle_map.find(id) == obstacle_map.end() ) {
            auto& footprint = obstacleposes[i].footprint;
            float x = std::abs(footprint.points[0].x) + d_approach;
            float y = std::abs(footprint.points[0].y) + d_approach;

            // initalize approach points in obstacle frame
            Obstacle obstacle;
            obstacle.Pts_O.col(0) <<  x, 0, 1;
            obstacle.Pts_O.col(1) << -x, 0, 1;
            obstacle.Pts_O.col(2) <<  0, y, 1;
            obstacle.Pts_O.col(3) <<  0,-y, 1;

            obstacle.length.resize(4);
            obstacle.length[0] = 2.0 * x;
            obstacle.length[1] = 2.0 * x;
            obstacle.length[2] = 2.0 * y;
            obstacle.length[3] = 2.0 * y;

            obstacle_map[id] = obstacle;
        }

        // update the approch points
        obstacle_map[id].TO_B = T;
        obstacle_map[id].Pts_B = T.matrix() * obstacle_map[id].Pts_O;
        obstacle_map[id].height = obstacleposes[i].height.data;
    }
}



