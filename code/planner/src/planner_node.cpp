#include "planner/planner.h"

#include <math.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "planner");
    ROS_INFO_STREAM("planner...");

    planner::Planner planner1("planner1");
    planner1.run();

    return 0;
}
