/**
 * @file obstacle_detector.h
 *
 * @brief base class for of planner
 *
 * @ingroup obstacle_detector
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */

#ifndef PLANNER_BASE_H
#define PLANNER_BASE_H

#include "planner/path_state.h"

#include <gridmap_2d/gridmap_2d.h>
#include <geometry_msgs/Pose2D.h>

#include <ros/node_handle.h>
#include <planner_msgs/plan.h>
#include <nav_msgs/Path.h>

#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <vector>

namespace planner
{

typedef std::vector<geometry_msgs::Pose2D> PathPose2D;

/**
 * Describes the base class for each planner
 *
 * generates a path based on given 2D occupiency grid.
 * The path is published as a nav_msgs::Path and the next goal is broadcasted
 * in the tf tree
 *
 * provides a service to generate a new path on demand
 *
 */
class PlannerBase
{
    enum State { CONSTRUCTED, INIT_GOAL, INIT_START, READY, PLANNED };

public:

    /**
    * @brief constructor
    *
    * @param name node frequency
    * @param nh ros nodehandle
    */
    PlannerBase(const std::string& name, const ros::NodeHandle& nh_ = ros::NodeHandle())
        : nh(nh_),
          name(name),
          map_updated(false),
          state(CONSTRUCTED)
    {
        // subscribers
        map_sub = nh.subscribe("map", 1, &PlannerBase::map_callback, this);
        // publisher
        path_pub = nh.advertise<nav_msgs::Path>(name + "/path", 1);
        // services
        plan_srv = nh.advertiseService(name + "/plan", &PlannerBase::plan_handle, this);
    }
    virtual ~PlannerBase()
    {
    }

    /**
    * @brief run planner (blocking)
    */
    bool run() {
        // initalize
        if( !init_request() ) {
            ROS_ERROR_STREAM("PlannerBase::init : error");
        }
        // run main loop
        ros::Rate loop_rate(rate);
        while (ros::ok()) {
            if( state == PLANNED) {
                geometry_msgs::Pose2D goal;
                if( get_goal_tf(goal) ) {
                    set_goal(goal);
                    //make_plan_request();
                    visualize_plan();
                }
            }
            ros::spinOnce();
            loop_rate.sleep();

        }
        return true;
    }

    /**
    * @brief initalize all parameters
    */
    bool init_request() {
        bool ret = true;
        if( !ros::param::get(name + "/rate", rate) ) {
            ROS_ERROR_STREAM("PlannerBase::init: rate");
            ret = false;
        }
        if( !ros::param::get(name + "/robot_frame", robot_frame) ) {
            ROS_ERROR_STREAM("PlannerBase::init: robot_frame");
            ret = false;
        }
        if( !init(nh) )
            ret = false;
        return ret;
    }

    /**
    * @brief generates a new path if goal and start pose were provieded
    */
    bool make_plan_request() {
        if( state == READY || state == PLANNED ) {
            state = PLANNED;
            state_path.clear();
            return make_plan(start_pose, goal_pose, grid_map, state_path);
        }
        return false;
    }

    /**
    * @brief set new goal position
    */
    bool set_goal(const geometry_msgs::Pose2D& goal_pose) {
        if( state == CONSTRUCTED )
            state = INIT_GOAL;
        else if( state == INIT_START )
            state = READY;
        this->goal_pose = goal_pose;
    }

    /**
    * @brief set new start positon
    */
    bool set_start(const geometry_msgs::Pose2D& goal_pose = geometry_msgs::Pose2D()) {
        if( state == CONSTRUCTED )
            state = INIT_START;
        else if( state == INIT_GOAL )
            state = READY;
        this->start_pose = start_pose;
    }

    PathStatePath get_plan() const {
        return state_path;
    }
    std::string get_name() const {
        return name;
    }
    std::string get_robot_frame() const {
        return robot_frame;
    }

    /**
    * @brief transforms the path form local /robot_frame into the /map frame
    */
    void transform_plan(const PathStatePath& state_path) {
        nav_msgs::Path path;
        geometry_msgs::PoseStamped pose;
        tf::Vector3 axis = tf::Vector3(0.0, 0.0, 1.0);
        tf::Quaternion quad;

        tf::StampedTransform transform, transform_base;
        listener.waitForTransform( "/map", robot_frame, ros::Time(0), ros ::Duration(0.2));
        try{
            listener.lookupTransform("/map", robot_frame, ros::Time(0), transform_base);
        }
        catch (tf::TransformException ex){
            ROS_ERROR_STREAM("Error tf transformation");
            return;
        }
        auto &t = transform_base.getOrigin();

        for( size_t i = 0; i < state_path.size(); ++i ) {
            pose.pose.position.x = state_path[i].p[0];
            pose.pose.position.y = state_path[i].p[1];
            pose.pose.position.z = 0.0;
            quad = tf::Quaternion(axis, state_path[i].th);
            pose.pose.orientation.x = quad.x();
            pose.pose.orientation.y = quad.y();
            pose.pose.orientation.z = quad.z();
            pose.pose.orientation.w = quad.w();
            path.poses.push_back(pose);

            if( i == 1 ) {
                transform.setRotation( quad );
                transform.setOrigin(tf::Vector3(state_path[i].p[0], state_path[i].p[1], 0.0));
                interm_goal = transform_base * transform;
                auto &t2 = interm_goal.getOrigin();
            }
        }
    }

    /**
    * @brief broadcast next intermediate goal
    */
    void visualize_plan() {
        if( state == PLANNED ) {
            br.sendTransform( tf::StampedTransform(interm_goal, ros::Time::now(), "/map", "x_0") );
        }
    }

private:
    /**
    * @brief abstract function to initalize all parameters
    */
    virtual bool init(ros::NodeHandle& nh) = 0;

    /**
    * @brief abstract function called if map was updated
    */
    virtual void map_changed(gridmap_2d::GridMap2DPtr map) = 0;

    /**
    * @brief abstract function called to generate a new path
    */
    virtual bool make_plan(const geometry_msgs::Pose2D& start_pose,
                           const geometry_msgs::Pose2D& goal_pose,
                           const gridmap_2d::GridMap2DPtr& map,
                           PathStatePath& state_path) = 0;

    /// @brief map callback
    void map_callback(nav_msgs::OccupancyGridConstPtr occupancy_map) {
        // delete old map and use new
        // map_updated = true;
        grid_map.reset(new gridmap_2d::GridMap2D(occupancy_map));
        map_changed(grid_map);
    }

    /// @brief service handle to generate a new path
    bool plan_handle(planner_msgs::planRequest& req, planner_msgs::planResponse& res) {
        bool ret = true;
        geometry_msgs::Pose2D goal, start;

        if( get_goal_tf(goal) ) {
            set_goal(goal);
            set_start(start);
            ret = make_plan_request();

            for(int i = 0; i < state_path.size(); ++i) {
                ROS_INFO_STREAM("x=" << state_path[i].p[0] << " y=" << state_path[i].p[1] << " th=" << state_path[i].th*180.0 / M_PI);
            }
            conv_path_msg( state_path, res);
            transform_plan(state_path);

            for(int i = 0; i < state_path.size(); ++i) {
                PathState s = state_path[i];
                std::cout << "i=" << i << "th:" << s.th << " id: " << s.id << " state: " << s.state << std::endl;
            }

            return ret;
        }
        return false;
    }

    bool get_goal_tf(geometry_msgs::Pose2D& goal) {
        tf::StampedTransform transform;
        listener.waitForTransform( "/map", "/base_footprint", ros::Time(0), ros ::Duration(0.2));
        try{
            listener.lookupTransform("/base_footprint", "/map" , ros::Time(0), transform);
        }
        catch (tf::TransformException ex){
            ROS_ERROR_STREAM("Error tf transformation");
            return false;
        }
        goal.x = transform.getOrigin().x();
        goal.y = transform.getOrigin().y();
        return true;
    }

    /**
     * convert a path to ros msg
     */
    void conv_path_msg(const PathStatePath& path, planner_msgs::planResponse& res) {
        auto& msg_path = res.path;
        msg_path.resize(path.size());
        for( size_t i = 0; i < path.size(); ++i) {
            msg_path[i].pose.x = path[i].p[0];
            msg_path[i].pose.y = path[i].p[1];
            msg_path[i].pose.theta = path[i].th;
            msg_path[i].state = path[i].state;
            msg_path[i].height = path[i].height;
            msg_path[i].length = path[i].length;
            msg_path[i].id = path[i].id;
        }
    }

private:
    State state;

    ros::NodeHandle nh;
    ros::ServiceServer plan_srv;            // plan service
    ros::Subscriber map_sub;                // get map updates
    ros::Publisher path_pub;                // get path

    std::string name, robot_frame;

    gridmap_2d::GridMap2DPtr grid_map;      // ptr to internal grid_map
    geometry_msgs::Pose2D goal_pose;
    geometry_msgs::Pose2D start_pose;

    PathStatePath state_path;               // special humanoid path

    tf::Transform interm_goal;

    tf::TransformListener listener;
    tf::TransformBroadcaster br;
    bool map_updated;
    double rate;

};

}

#endif // PLANNER_BASE_H
