/**
 * @file path-state.h
 *
 * @brief description of a point along a path
 *
 * @ingroup planner
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */


#ifndef PATH_STATE_H_
#define PATH_STATE_H_

#include <Eigen/Dense>
#include <vector>
#include <planner_msgs/path.h>

namespace planner
{


/**
 * Storage class to hold all information used to describe a single point
 * along a path
 *
 * Each point is desribes by its position, orientation theta
 * Additional information used by the planner is state,
 * obstacle height, obstacle length and obstacle id
 *
 */
class PathState
{
public:
    enum State { MOVE = planner_msgs::posestate::STATE_MOVE,
                 APPROACH = planner_msgs::posestate::STATE_APPROACH,
                 STEP = planner_msgs::posestate::STATE_STEP };

    /**
    * @brief default constructor
    */
    PathState()
        : th(0.0f),
          height(0.0f),
          length(0.0f),
          id(0)
    {
        p.setZero();
        state = MOVE;
    }

    /**
    * @brief constructor
    *
    * @param p 2D position
    * @param th orientation vector in 2d plane
    * @param state of the action that must executed in this step
    * @param height of an obstacle
    * @param length of an obstacle
    * @param id of an obstacle
    */
    PathState(const Eigen::Vector2f& p, float th, float height = 0.0f, float length = 0.0f, int id = 0, State state = MOVE)
        : p(p),
          th(th),
          state(state),
          height(height),
          length(length),
          id(id)
    {
    }

    /**
    * @brief copy constructor
    */
    PathState(const PathState& pathstate)
        : p(pathstate.p),
          th(pathstate.th),
          state(pathstate.state),
          height(pathstate.height),
          length(pathstate.length),
          id(pathstate.id)
    {
    }

public:
    Eigen::Vector2f p;
    float th;
    float height;
    float length;
    int id;
    State state;

};
typedef std::vector<PathState> PathStatePath;

}

#endif
