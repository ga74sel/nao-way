#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <eigen_conversions/eigen_msg.h>
#include <opencv2/core/eigen.hpp>
#include <geometry_msgs/Pose2D.h>

namespace math
{
    /**
     * @brief convert cv::Mat translation, orientation to Eigen::Transform
     * @param t_vec: 3x1 tanslation
     * @param r_vec: 3x1 angle axis rotation vector
     * @param T: homogeneous transformation
     */
    inline void cvpose2eigen(const cv::Mat& t_vec, const cv::Mat& r_vec, Eigen::Affine3d& T)
    {
        /*Eigen::Vector3d t, r;

        cv::cv2eigen(t_vec, t);
        cv::cv2eigen(r_vec, r);

        double theta = r.norm();
        T = Eigen::Translation3d(t) * Eigen::AngleAxisd(theta, r / theta );*/

        cv::Mat R_cv;
        cv::Rodrigues(r_vec, R_cv);

        Eigen::Matrix3d R;
        Eigen::Vector3d t;
        cv::cv2eigen(R_cv, R);
        cv::cv2eigen(t_vec, t);

        T.matrix() << R, t, 0, 0, 0, 1;
    }

    /**
     * @brief set euler angles of Eigen::Matrix
     * @param m: 3x3 eigen matrix
     * @param yaw: rotation angle around z
     * @param pitch: rotation angle around y
     * @param roll: rotation angle around x
     */
    inline void eigenSetEulerYPR(Eigen::Matrix3d& m, double yaw, double pitch, double roll)
    {
        m = Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ())
                * Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX())
                * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY());
    }

    /**
     * @brief get euler angles of Eigen::Matrix
     * @param m: 3x3 eigen matrix
     * @param yaw: rotation angle around z
     * @param pitch: rotation angle around y
     * @param roll: rotation angle around x
     * @param fixed: rotation around fixed frame (premultipy)
     */
    inline void eigenGetEulerYPR(const Eigen::Matrix3d& m, double& yaw, double& pitch, double& roll, bool fixed = false)
    {
        struct Euler {
            tfScalar yaw;
            tfScalar pitch;
            tfScalar roll;
        };
        Euler euler_out;
        Euler euler_out2;

        // Check that pitch is not at a singularity
        if (std::abs(m(2,0)) >= 1)
        {
                euler_out.yaw = 0;
                euler_out2.yaw = 0;

                // From difference of angles formula
                if (m(2,0) < 0) {
                    tfScalar delta = std::atan2(m(0,1),m(0,2));
                    euler_out.pitch = M_PI / 2.0;
                    euler_out2.pitch = M_PI / 2.0;
                    euler_out.roll = delta;
                    euler_out2.roll = delta;
                }
                else {
                    tfScalar delta = std::atan2(-m(0,1),-m(0,2));
                    euler_out.pitch = -M_PI / 2.0;
                    euler_out2.pitch = -M_PI / 2.0;
                    euler_out.roll = delta;
                    euler_out2.roll = delta;
                }
        } else {
                euler_out.pitch = - std::asin(m(2,0));
                euler_out2.pitch = M_PI - euler_out.pitch;

                euler_out.roll = std::atan2(m(2,1) / std::cos(euler_out.pitch),
                        m(2,2) / std::cos(euler_out.pitch));
                euler_out2.roll = std::atan2(m(2,1) / std::cos(euler_out2.pitch),
                        m(2,2) / std::cos(euler_out2.pitch));

                euler_out.yaw = std::atan2(m(1,0) / std::cos(euler_out.pitch),
                        m(0,0) / std::cos(euler_out.pitch));
                euler_out2.yaw = std::atan2(m(1,0) / std::cos(euler_out2.pitch),
                        m(0,0) / std::cos(euler_out2.pitch));
        }
        if (!fixed) {
                yaw = euler_out.yaw;
                pitch = euler_out.pitch;
                roll = euler_out.roll;
        } else {
                yaw = euler_out2.yaw;
                pitch = euler_out2.pitch;
                roll = euler_out2.roll;
        }
    }


    /**
     * @brief convert Eigen::Transform into a geometry::pose 2D
     * @param T: 4x4 Transformation matrix
     * @param pose: geometry::Pose2D
     * @param axis: axis that points upward ( x=0, y=1, z=2 )
     */
    inline void eigen2Pose2D(const Eigen::Affine3d& T, geometry_msgs::Pose2D& pose, int axis = 2)
    {
        double yaw, pitch, roll;
        eigenGetEulerYPR(T.rotation(), yaw, pitch, roll);

        switch(axis) {
            case 0:
                pose.theta = pitch;
                pose.x = T.translation()(2);
                pose.y = T.translation()(1);
                break;
            case 1:
                pose.theta = roll;
                pose.x = T.translation()(0);
                pose.y = T.translation()(2);
                break;
            case 2:
                pose.theta = yaw;
                pose.x = T.translation()(0);
                pose.y = T.translation()(1);
                break;
        }
    }

    /**
     * @brief convert Eigen::Transform into a geometry::pose 2D
     * @param T: 4x4 Transformation matrix
     * @param pose: geometry::Pose2D
     * @param axis: axis that points upward ( x=0, y=1, z=2 )
     */
    inline void eigen2Pose(const Eigen::Affine3d& T, geometry_msgs::Pose& pose)
    {
        // position
        pose.position.x = T.translation()(0);
        pose.position.y = T.translation()(1);
        pose.position.z = T.translation()(2);

        // orientation
        Eigen::Quaterniond q_eigen(T.rotation());
        pose.orientation.w = q_eigen.w();
        pose.orientation.x = q_eigen.x();
        pose.orientation.y = q_eigen.y();
        pose.orientation.z = q_eigen.z();
    }
}
