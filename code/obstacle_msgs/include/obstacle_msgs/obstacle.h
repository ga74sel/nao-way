/**
 * @file obstacle.h
 *
 * @brief desription of an obstacle
 *
 * @ingroup obstacle_msgs
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */


#ifndef OBSTACLES_H_
#define OBSTACLES_H_

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <geometry_msgs/Polygon.h>

namespace obstacle_msgs
{

/**
* Container to store all relevant obstacle parameter
*/
class Obstacle
{
public:
    Obstacle()
    {
    }


    /** ObstacleDetector
    *
    * @param id aruco marker id
    * @param w width in marker y direction
    * @param h heigth in marker x direction
    * @param d depth in marker z direction
    */
    Obstacle(int id, double w, double h, double d, const Eigen::Affine3d& TCP_M)
        : id(id),
          TCP_M(TCP_M),
          w(w),
          h(h),
          d(d)
    {
        build_footprints();
    }

    Eigen::Affine3d T_raw;                  // raw aruco Transformation
    Eigen::Affine3d TCP_M;                  // transformation center to marker
    Eigen::Affine3d T;                      // transformation of object
    int id;                                 // marker id
    double w, h, d;                         // dimensions ( w in x, h in y, d in z )
    int main_axis;                          // main rotation axis
    double theta;                           // rotation angle around main axis

    geometry_msgs::Polygon footprints[3];   // footprint in each axis
    double height[3];                       // height of the obstacle

    ros::Time time;                         // last visible time

    void build_footprints() {
        // x-axis to top
        footprints[0].points.resize(4);
        footprints[0].points[0].x = -d/2; footprints[0].points[0].y = -w/2;
        footprints[0].points[1].x = d/2;  footprints[0].points[1].y = -w/2;
        footprints[0].points[2].x = d/2;  footprints[0].points[2].y = w/2;
        footprints[0].points[3].x = -d/2; footprints[0].points[3].y = w/2;

        // y-axis to top
        footprints[1].points.resize(4);
        footprints[1].points[0].x = -h/2; footprints[1].points[0].y = -d/2;
        footprints[1].points[1].x = h/2;  footprints[1].points[1].y = -d/2;
        footprints[1].points[2].x = h/2;  footprints[1].points[2].y = d/2;
        footprints[1].points[3].x = -h/2; footprints[1].points[3].y = d/2;

        // z-axis to top
        footprints[2].points.resize(4);        
        footprints[2].points[0].x = -h/2; footprints[2].points[0].y = -w/2;
        footprints[2].points[1].x = h/2;  footprints[2].points[1].y = -w/2;
        footprints[2].points[2].x = h/2;  footprints[2].points[2].y = w/2;
        footprints[2].points[3].x = -h/2; footprints[2].points[3].y = w/2;

        height[0] = h;
        height[1] = w;
        height[2] = d;
    }
};

typedef std::map<int, obstacle_msgs::Obstacle> Obstacles;

}

#endif
