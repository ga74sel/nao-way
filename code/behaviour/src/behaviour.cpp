#include "behaviour/behaviour.h"

#define DETECTION_TIME  1.1
#define APPROACH_DIST   0.125

// delta criterion for approaching
#define DELTA_Y         0.02
#define DELTA_X         0.013
#define DELTA_TH        0.15


// shift away from obstacle center
#define SHIFT_Y         0.028

using namespace behaviour;

Behaviour::Behaviour()
        : is_busy(false),
          timer_flag(false),
          state(IDLE),
          robot_frame("base_footprint")
{
    // Publisher
    success_pub = nh.advertise<std_msgs::Bool>("/behaviour/success", 1);

    // ServiceServer
    start_srv = nh.advertiseService("/behaviour/approaching", &Behaviour::start_approaching_cb, this);

    // ServiceClient
    detect_client = nh.serviceClient<obstacle_msgs::detect>("/obstacledetector/detect");
    head_client = nh.serviceClient<head_manager::MoveJoints>("/move_joint");
    walk_client = nh.serviceClient<walk_manager::WalkPose>("walk_manager/walk_slow");

    // Timer
    timer = nh.createTimer(1.0/DETECTION_TIME, &Behaviour::timer_cb, this);
}


void Behaviour::update() {

    state_transition();
}


void Behaviour::state_transition() {

    switch(state) {

    case IDLE: {
        if(is_busy) {
            state = TURN_HEAD;
            head_turn_count = 0;
        }
      break;
    }

    case TURN_HEAD: {

        if(timer_flag) {
            double yaw = (head_turn_count) ? -0.25 : 0.25;
            turn_head(yaw, 0.65, 0.4);
            head_turn_count++;
            state = SEARCH_MARKER;
            timer.stop();
            timer.start();
        }
      break;
    }

    case SEARCH_MARKER: {

        if(timer_flag) {
            // detect obstacles
            obstacle_msgs::detect srv;
            detect_client.call(srv);
            transform_obstacle(srv.response);

            /*if(transform_obstacle(srv.response)) {
                if(obstacle.X_B[1] > 0) {
                    // to the left, pos rot
                    ROS_INFO_STREAM("pos:" << obstacle.X_B[0] << " " << obstacle.X_B[1]);
                }
                else {
                    // to the right, pos neg
                    ROS_INFO_STREAM("pos:" << obstacle.X_B[0] << " " << obstacle.X_B[1]);
                }

            }*/

            ROS_INFO_STREAM("DETECT!");
            if(head_turn_count == 2) {
                state = ADJUST_TH;
            }
            else
                state = TURN_HEAD;

            timer.stop();
            timer.start();
        }
        else
        { ROS_INFO_STREAM("wait!!"); }
      break;
    }

    case ADJUST_TH: {

        // 1. compute target
        compute_goal(goal);
        goal.x = goal.y =0.0;

        // 2. walk to target
        if(std::abs(goal.theta) > DELTA_TH ) {
            //goal.theta += (std::signbit(goal.theta)) ? -DELTA_TH/2 : DELTA_TH/2;
            ROS_INFO_STREAM("TH: " << goal);
            walk_to(goal);
            state = WALKING_DONE;
        }
        else {
            state = ADJUST_Y;
        }
      break;
    }

    case ADJUST_Y: {

        // 1. compute target
        compute_goal(goal);
        goal.x = goal.theta = 0.0;

        // 2. walk to target
        if(std::abs(goal.y) > DELTA_Y ) {
            //goal.y += (std::signbit(goal.y)) ? -DELTA_Y/2 : DELTA_Y/2;
            ROS_INFO_STREAM("Y: " << goal);
            walk_to(goal);
            state = WALKING_DONE;
        }
        else {
            state = ADJUST_X;
        }

      break;
    }

    case ADJUST_X: {

        // 1. compute target
        compute_goal(goal);
        goal.y = goal.theta = 0.0;

        // 2. walk to target
        if(goal.x > DELTA_X ) {
            goal.x += (std::signbit(goal.x)) ? -DELTA_X/2 : DELTA_X/2;

            if(obstacle_id == 6)
                goal.x -= 0.015;

            ROS_INFO_STREAM("X: " << goal);
            walk_to(goal);
            state = WALKING_DONE;
        }
        else {
            state = IDLE;
            is_busy=false;
            ROS_INFO_STREAM("behaviour: Approaching done!");
            pub_success(true);

            // reset head position
            turn_head(0, 0, 0.3);
        }

      break;
    }

    case WALKING_DONE: {

        // end condition!

            state = TURN_HEAD;
            head_turn_count = 0;
            timer.stop();
            timer.start();
      break;
    }

    }

    // reset timer_flag
    timer_flag = false;
}

// -------------------
// AUXILIARY FUNCTIONS
// -------------------


void Behaviour::compute_goal(geometry_msgs::Pose2D &p) {

    Eigen::Vector2d x;
    const Eigen::Matrix<double,3,4>& P = obstacle.Pts_B;

    double d_min = std::numeric_limits<double>::infinity();
    for(size_t i = 0; i < 4; ++i) {
        double d = (P.col(i).head(2)).norm();
        if( d < d_min ) {
            d_min = d;
            x = P.col(i).head(2);
        }
    }

    Eigen::Rotation2D<double> R(0);
    R.fromRotationMatrix(obstacle.TO_B.rotation().matrix());
    double th = R.angle();

    while( th > M_PI / 4.0 )
        th -= M_PI / 2.0;
    while( th < -M_PI / 4.0)
        th += M_PI / 2.0;

    p.x = x(0);
    p.y = x(1) - SHIFT_Y;
    p.theta = th;
}


void Behaviour::walk_to(geometry_msgs::Pose2D &p){

    walk_manager::WalkPose srv;
    ROS_INFO_STREAM("walk_to: " << p.x << ", " << p.y << ", " << p.theta);
    srv.request.pose = p;
    walk_client.call(srv);
}


void Behaviour::turn_head(float yaw, float pitch, float time) {

    ros::service::waitForService("move_joint");
    head_manager::MoveJoints srv;
    srv.request.joint_names = { "HeadYaw", "HeadPitch" };
    srv.request.time_list = {time, time};
    srv.request.joint_angles = {yaw, pitch};
    srv.request.use_interpolation = 1;
    head_client.call(srv);
}


void Behaviour::pub_success(bool success) {
    std_msgs::Bool msg;
    msg.data = success;
    success_pub.publish(msg);
}


double Behaviour::norm(const geometry_msgs::Pose2D& p) {
    return sqrt(p.x*p.x + p.y*p.y);
}


bool Behaviour::transform_obstacle(obstacle_msgs::detectResponse &res) {

    bool obstacle_detected = false;
    auto& obstacleposes = res.obstacleposes;
    std::string obstacle_frame = res.header.frame_id;

    // transformations
    Eigen::Affine2d TO;         // 2D transformation obstacle wrt obstacle frame
    Eigen::Affine2d TO_B;       // 2D transformation obstacle wrt base frame
    Eigen::Affine3d TO_B_3D;    // 3D transformation obstacle wrt base frame

    // get transformation obstacle_frame wrt robot_frame
    tf::StampedTransform transform;
    listener.waitForTransform(robot_frame, obstacle_frame, ros::Time(0), ros ::Duration(0.2));
    try {
        listener.lookupTransform(robot_frame, obstacle_frame, ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("Behaviour: no transformation");
        return false;
    }
    tf::transformTFToEigen(transform, TO_B_3D);
    TO_B = Eigen::Translation2d(TO_B_3D.translation().head(2)) * TO_B_3D.linear().topLeftCorner(2,2);


    // extract obstacle of interest
    for( int i = 0; i < obstacleposes.size(); ++i) {
        int id = obstacleposes[i].id.data;
        if( id != obstacle_id )
            continue;

        // find the transformation obstacle wrt base
        auto& pose = obstacleposes[i].pose;
        Eigen::Vector2d position;
        position << pose.x, pose.y;
        TO = Eigen::Translation2d(position) * Eigen::Rotation2D<double>(pose.theta);

        auto& footprint = obstacleposes[i].footprint;
        float x = std::abs(footprint.points[0].x) + APPROACH_DIST;
        float y = std::abs(footprint.points[0].y) + APPROACH_DIST;

        // initalize approach points in obstacle frame
        obstacle.Pts_O.col(0) <<  x, 0, 1;
        obstacle.Pts_O.col(1) << -x, 0, 1;
        obstacle.Pts_O.col(2) <<  0, y, 1;
        obstacle.Pts_O.col(3) <<  0,-y, 1;

        // update the approch points
        obstacle.TO_B = TO_B * TO;
        obstacle.Pts_B = obstacle.TO_B.matrix() * obstacle.Pts_O;

        // update the marker position
        Eigen::Vector3d X;
        X << obstacleposes[i].marker.x, obstacleposes[i].marker.y, 1;
        ROS_INFO_STREAM("inside: " << obstacleposes[i].marker.x << " " << obstacleposes[i].marker.y );

        obstacle.X_B = (TO_B.matrix() * X).head(2);
        obstacle_detected = true;
        ROS_INFO_STREAM("---> obstacle updated!");
    }
    return obstacle_detected;
}

// --------
// CALLBACK
// --------

void Behaviour::timer_cb(const ros::TimerEvent&) {
    timer_flag = true;
}

bool Behaviour::start_approaching_cb(behaviour::Approach::Request &req, behaviour::Approach::Response &res) {
    is_busy = true;
    // get id of obstacle that needs to be approached
    obstacle_id = req.id;
    return true;
}
