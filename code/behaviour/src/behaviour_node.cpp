#include "ros/ros.h"
#include "behaviour/behaviour.h"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "behaviour_node");
    ROS_INFO_STREAM("behaviour_node...");

    behaviour::Behaviour behaviour;
    ros::Rate loop_rate(5);

    while(ros::ok())
    {
        behaviour.update();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
