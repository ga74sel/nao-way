###
 # @file steppign_down.h
 #
 # @brief advertise srv to step down an obstacle
 #
 # @ingroup behaviour
 #
 # @author group_c
 # Contact: group_c@tum.c
 #
###

#!/usr/bin/env python

import rospy
import std_srvs
import motion
import almath
import argparse
import math
from naoqi import ALProxy
import sys
import time
from behaviour.srv import *
from std_msgs.msg import Bool

from move_operation import *

class StepUp(object):

    def __init__(self, robotIP, PORT=9559):
        self.motionProxy  = ALProxy("ALMotion", robotIP, PORT)
        self.postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)

        self.srv = rospy.Service("behaviour/step_down", StepHeight, self.handler)
        self.pub = rospy.Publisher("behaviour/success", Bool, queue_size=1)

    def handler(self, req):

        height = req.height.data
        print("height: " + str(height))

        motionProxy = self.motionProxy
        postureProxy = self.postureProxy

        # send NAO to Pose Init
        self.postureProxy.goToPosture("StandInit", 0.4)
        motionProxy.setStiffnesses("Body", 1.0)
        motionProxy.wbEnable(True)

        # ballance on RLeg
        balance_on(motionProxy, "RLeg", 1.5)
        motionProxy.wbEnable(False)

        op = MoveOperation(motionProxy)

    ############################ Step I #####################################
        print("Rotate left leg 90 degree and lift it up")
        #op = arms_right(1, op)
        deltaTfL = almath.Transform(0.0, 0.0, 0.025)*almath.Transform().fromRotZ(90.0*almath.TO_RAD)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        deltaTfT = almath.Transform(0.0, 0.0, 0.0)*almath.Transform().fromRotY(5*almath.TO_RAD)
        op.delta_tf(["LLeg", "RLeg", "Torso"], [deltaTfL, deltaTfR, deltaTfT], 2)
        op.execute()
        time.sleep(0.5)

        print("Put left foot near the bar")
        deltaTfL  = almath.Transform(0.015, -0.05, 0)
        deltaTfR  = almath.Transform(0.0, 0.0, 0.0)
        deltaTfT  = almath.Transform(0.02, -0.04, -0.03)
        op.delta_tf(["LLeg", "RLeg", "Torso"], [deltaTfL, deltaTfR, deltaTfT], 4)
        op.execute()
        deltaTfL  = almath.Transform(0.0, 0.0, -0.02)
        op.delta_tf(["LLeg"], [deltaTfL], 2)
        op.execute()
        time.sleep(0.5)

        print("Lift the torso")
        deltaTfT  = almath.Transform(0.01, 0.01, 0.02)

        deltaTfL = almath.Transform(-0.01, 0.02, 0.0)#*almath.Transform().fromRotZ(-30.0*almath.TO_RAD)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        op.delta_tf(["LLeg","Torso", "RLeg"], [deltaTfL, deltaTfT, deltaTfR], 4)
        op.execute()


        print("Move the mass center along to the left leg")
        deltaTfT  = almath.Transform(0.05, 0.05, -0.01)#
        deltaTfL = almath.Transform(0.0, 0.0, 0.0)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        op.delta_tf(["LLeg","Torso", "RLeg"], [deltaTfL, deltaTfT, deltaTfR], 4)
        op.execute()


        print("Move the mass center above the Left foot")

        deltaTfL = almath.Transform(0.0, 0.0, 0.0)
        #deltaTfT  = almath.Transform(0.03, 0.08, 0.0)
        deltaTfT  = almath.Transform(0.08, 0.03, 0.0)
        #deltaTfR = almath.Transform(0.03, 0.0, 0.03)
        deltaTfR = almath.Transform(-0.03, -0.02, 0.05)
        op.delta_tf(["LLeg","Torso", "RLeg"], [deltaTfL, deltaTfT, deltaTfR], 6)
        op.execute()
        op.move_angle(['RShoulderPitch', 'LShoulderRoll', 'LElbowYaw'], [-0.7, -0.9, 0.6], 1) # move arm adjust mass center
        op.move_angle(['LHipPitch', 'LHipRoll'], [-0.3, -0.3], 4)

        print("Raise the right leg")
        op.move_angle(['RShoulderPitch', 'LShoulderRoll', 'LElbowYaw'], [0.7, 0.3, -0.3], 1) # move arm back 0.9, -0.6
        op.move_angle(['RHipRoll'], [-0.5], 1)
        op.move_angle(['RHipPitch', 'RAnklePitch', 'RKneePitch'], [-1.0, 0.85, 0.2], 4)
        op.move_angle([ 'LShoulderRoll', 'LElbowYaw'], [0.6, -0.3], 4) # move arm back

        print("execute the stepping stepping down of right leg")
        print("step 1")
        op.move_angle(['LKneePitch', 'LAnklePitch'], [0.3, -0.07], 4)

        deltaTfT  = almath.Transform(-0.03, 0.0, 0.0)
        op.delta_tf(["Torso",], [deltaTfT], 4)
        op.execute()

        print("step 2")
        op.move_angle(['LKneePitch', 'LAnklePitch'], [0.3, -0.07], 4)

        print("step 3")
        op.move_angle(['RShoulderRoll', 'RElbowYaw', 'RElbowRoll'], [1.1, -1.2, 0.4], 1)
        op.move_angle(['LKneePitch', 'LAnklePitch', 'LAnkleRoll', 'LShoulderRoll', 'LElbowRoll'], [0.25, -0.3, 0, 0.6, 0.8], 5)
        ################ Variable ##################################
        print("step final")
        deltaTfR  = almath.Transform(0.0, 0.0, -height)
        deltaTfL  = almath.Transform(0.0, 0.0, 0.0)
        deltaTfT  = almath.Transform(0.0, 0.0, -height)
        op.delta_tf(['RLeg', 'LLeg', 'Torso'], [deltaTfR, deltaTfL, deltaTfT], 4)
        op.execute()
        ################ Variable ##################################
        #raw_input('press enter')
        op.move_angle(['RShoulderRoll', 'RElbowYaw', 'RElbowRoll', 'LShoulderRoll',
        'LElbowRoll', 'RShoulderPitch'], [-0.6, -0.8, -1.1, 1.2, -0.4, -0.2], 1)
        op.move_angle(['RShoulderPitch'], [0.3], 1)
        #op.move_angle(['RShoulderRoll', 'LShoulderRoll'], [0.6, -0.6], 5)
        #raw_input('press enter')

    ############################ Step I #####################################




    ################ Joint States Of Step I ############################
    #    op.move_angle_abs(['HeadYaw', 'HeadPitch', 'LShoulderPitch', 'LShoulderRoll', 'LElbowYaw',
    #    'LElbowRoll', 'LWristYaw', 'LHand', 'LHipYawPitch', 'LHipRoll', 'LHipPitch', 'LKneePitch',
    #    'LAnklePitch', 'LAnkleRoll', 'RHipYawPitch', 'RHipRoll', 'RHipPitch', 'RKneePitch', 'RAnklePitch',
    #    'RAnkleRoll', 'RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll', 'RWristYaw', 'RHand'],
    #    [-0.009245872497558594, 0.027570009231567383, 1.3038580417633057, 1.2056820392608643,
    #    -1.4082541465759277, -0.5844120979309082, 0.0030260086059570312, 0.25279998779296875,
    #    -0.8789401054382324, -0.37885594367980957, 0.08594608306884766, 1.4818021059036255,
    #    -0.7839159965515137, 0.19002999365329742, -0.8789401054382324, -0.7899680137634277,
    #    -0.44336795806884766, 0.6136419773101807, 0.3528618812561035, 0.29917192459106445,
    #    1.0999197959899902, -0.5108640193939209, -0.3958139419555664, 0.21326804161071777,
    #    0.0060939788818359375, 0.25199997425079346], 10)
    #    raw_input('press enter')

    #    op.move_angle_abs(['HeadYaw', 'HeadPitch', 'LShoulderPitch', 'LShoulderRoll', 'LElbowYaw',
    #    'LElbowRoll', 'LWristYaw', 'LHand', 'LHipYawPitch', 'LHipRoll', 'LHipPitch', 'LKneePitch',
    #    'LAnklePitch', 'LAnkleRoll', 'RHipYawPitch', 'RHipRoll', 'RHipPitch', 'RKneePitch', 'RAnklePitch',
    #    'RAnkleRoll', 'RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll', 'RWristYaw', 'RHand'],
    #    [0.004559993743896484, 0.027570009231567383, 1.3053920269012451, 1.1995460987091064,
    #    -1.4051861763000488, -0.23619413375854492, 0.013764142990112305, 0.24879997968673706,
    #    -0.8804740905761719, -0.37885594367980957, 0.07520794868469238, 1.5232200622558594,
    #    -0.8099939823150635, 0.18071642518043518, -0.8804740905761719, -0.7424139976501465,
    #    -0.49859189987182617, 0.6796040534973145, 0.3206479549407959, 0.2684919834136963,
    #    1.129065990447998, 0.0014920234680175781, 0.43561410903930664, 1.2717280387878418,
    #    -0.019984006881713867, 0.2491999864578247], 10)
    #    raw_input('press enter')

    ################ Joint State Of Step I ############################




    ############################ Step II #####################################
        print("Move the mass center along to the right leg")
        ################ Variable ##################################
        op.move_angle(['LShoulderRoll', 'RShoulderRoll'], [-1.3, -0.3], 1)
        #deltaTfT  = almath.Transform(0.03, -0.09, 0.04) #0.045
        #deltaTfT  = almath.Transform(0.03, -0.14, 0.04) #0.027
        deltaTfT  = almath.Transform(0.03, 2.78*height-0.215, 0.04)
        ################ Variable ##################################

        deltaTfL = almath.Transform(0.0, 0.0, 0.0)#*almath.Transform().fromRotZ(-30.0*almath.TO_RAD)
        deltaTfR = almath.Transform(-0.04, 0.0, 0.0)
        op.delta_tf(["LLeg","Torso", "RLeg"], [deltaTfL, deltaTfT, deltaTfR], 7)
        op.execute()
        time.sleep(0.5)
        #raw_input('press enter')

        print("Move the mass center above the right leg")
        #op.move_angle(['RShoulderRoll'], [-0.8], 2) # 0.045 -0.2
        op.move_angle(['RHipRoll', 'RHipPitch'], [-0.2, -0.2], 6) # 0.045 -0.2
        #op.move_angle(['RShoulderRoll'], [0.8], 2) # 0.045 -0.2
        #op.move_angle(['RHipRoll', 'RHipPitch'], [-0.3, -0.3], 6) # 0.027
        time.sleep(0.5)
        #raw_input('press enter')

        print("Rotating the Left Leg")
        deltaTfL = almath.Transform(0.06, -0.02, 0.02)*almath.Transform().fromRotZ(-86.0*almath.TO_RAD)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        deltaTfT = almath.Transform().fromRotY(-15.0*almath.TO_RAD)
        op.delta_tf(["LLeg", "RLeg", "Torso"], [deltaTfL, deltaTfR, deltaTfT], 6)
        op.execute()
        time.sleep(0.5)
        #raw_input('press enter')


        print("Adjusting the Left foot horizontal to the ground")
        ################ Variable ##################################
        #op.move_angle(['LHipPitch', 'LAnklePitch'], [-0.7, 13.88*height+0.075], 5)
        op.move_angle(['LHipPitch', 'LAnklePitch'], [-11.76*height-0.36,
        13.88*height+0.075], 5)
        ################ Variable ##################################
        time.sleep(0.5)
        #raw_input('press enter')


        print("Left leg touching the ground, Torso to the left")
        deltaTfT = almath.Transform(0.0, -0.04, 0.0)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        ################ Variable ##################################
        #deltaTfL = almath.Transform(0.0, -0.02, -0.03) #0.045
        #deltaTfL = almath.Transform(0.0, -0.02, -0.01) #0.027
        deltaTfL = almath.Transform(0.0, -0.02, -1.1*height+0.0197-0.005)
        ################ Variable ##################################

        op.delta_tf(["LLeg", "RLeg"], [deltaTfL, deltaTfR], 2)
        op.execute()
        op.move_angle(['RHipPitch'], [0.15], 3)
        postureProxy.goToPosture("StandInit", 0.3)
    ############################ Step II #####################################

        # publish success
        self.pub.publish(True)

        return StepHeightResponse()


if __name__ == "__main__":
    robotIP=str(sys.argv[1])
    PORT=int(sys.argv[2])
    rospy.init_node('stepping_up_srv_node')

    step_up = StepUp(robotIP, PORT)

    rospy.spin()

