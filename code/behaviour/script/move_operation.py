###
 # @file move_operation.h
 #
 # @brief advertise srv to move to desired goal
 #
 # @ingroup behaviour
 #
 # @author group_c
 # Contact: group_c@tum.c
 #
###

#!/usr/bin/env python

import motion
import almath
from naoqi import ALProxy
import time


class MoveOperation(object):
    """
    This class that encapsulates the a movement operation.
    It enables easily appending movement operations in operational space or joint space	
    """
    def __init__(self, motionProxy):
        self.motionProxy = motionProxy
        self.clear()

    def clear(self):
 	"""
	clears all operations
	"""
        self.effectors = []
        self.frames = []
        self.paths = []
        self.axisMasks = []
        self.times = []

    def delta_tf(self, effector, deltaTf, time, frame=motion.FRAME_WORLD, axisMask=almath.AXIS_MASK_ALL):
	"""
	adds a delta transform to the movement operation
        """
        path = []
        if type(effector) is list:
            for i in range(len(effector)):
                initTf = almath.Transform(self.motionProxy.getTransform(effector[i], frame, False))
                targetTf = initTf*deltaTf[i]
                path.append(list(targetTf.toVector()))
        else:
            initTf = almath.Transform(self.motionProxy.getTransform(effector, frame, False))
            targetTf = initTf*deltaTf
            path = list(targetTf.toVector())

        self.append(effector, frame, path, axisMask, time)

    def append(self, effector, frame, path, axisMask, time):
	"""
	appends a operation to all required class lists (depending on the input type, e.g. 'list')
        """
        if type(effector) is list:
            l = len(effector)
            frame = self.assert_list(frame, l)
            path = self.assert_list(path, l)
            axisMask = self.assert_list(axisMask, l)
            time = self.assert_list(time, l)

            for i in range(len(effector)):
                self.effectors.append(effector[i])
                self.frames.append(frame[i])
                self.paths.append(path[i])
                self.axisMasks.append(axisMask[i])
                self.times.append(time[i])
        else:
            self.effectors.append(effector)
            self.frames.append(frame)
            self.paths.append(path)
            self.axisMasks.append(axisMask)
            self.times.append(time)

    def assert_list(self, item, l):
        if type(item) is list:
            if len(item) == l:
                return item
            else:
                return [item]*l
        else:
            return [item]*l

    def move_angle(self, joint_name, angle, time):
	"""
	moves the joints 'joint_name' in a certain 'angle' in the time
	"""
        taskList = self.motionProxy.getTaskList()
        for taskid in taskList:
            self.motionProxy.killTask(taskid[1])
        self.motionProxy.angleInterpolation(joint_name, angle, time, False)

    def move_angle_abs(self, joint_name, angle, time):
        taskList = self.motionProxy.getTaskList()
        for taskid in taskList:
            self.motionProxy.killTask(taskid[1])
        self.motionProxy.angleInterpolation(joint_name, angle, time, True)

    def execute_transform(self, Name, path, duration):
        self.motionProxy.transformInterpolations(Name, 0, path, 63, duration, True)

    def get_transformation(self, Name):
        self.motionProxy.getTransform(Name, 0, False)


    def execute(self):
	"""
        exeutes the current operation in the class list and subsequently clears the operation buffers
        """
        self.motionProxy.transformInterpolations(self.effectors, self.frames, self.paths, self.axisMasks, self.times)
        self.clear()


# _________________________________________________________________________________________________


def balance_on(motionProxy, supportLeg, duration):
    """
    balance on 'supportLeg' within 'duration' using the ALProxy balancer
    """
    motionProxy.wbGoToBalance(supportLeg, duration)


# _________________________________________________________________________________________________


def arms_left(time, op):
    """
    function that adds the motion "arms_left" to the movement operation op
    """
    if op is None:
        op = MoveOperation()
    return op

    # define tf
    lTf = almath.Transform(0.17, 0.19, 0.0)
    rTf = almath.Transform(0.11, -0.01, 0.0)

    # extend movement operation op
    effector = ["LArm", "RArm"]
    frame      = motion.FRAME_TORSO
    path = [list(lTf.toVector()), list(rTf.toVector())]
    axisMask   = almath.AXIS_MASK_VEL   # only position

    op.append(effector, frame, path, axisMask, time)

    return op


# _________________________________________________________________________________________________


def arms_right(time, op):
    """
    function that adds the motion "arms_right" to the movement operation op
    """
    # define tf
    if op is None:
        op = MoveOperation()
    return op

    rTf = almath.Transform(0.17, -0.19, 0.0)
    lTf = almath.Transform(0.11, -0.01, 0.0)

    # extend movement operation op
    effector = ["LArm", "RArm"]
    frame      = motion.FRAME_TORSO
    path = [list(lTf.toVector()), list(rTf.toVector())]
    axisMask   = almath.AXIS_MASK_VEL   # only position

    op.append(effector, frame, path, axisMask, time)

    return op
