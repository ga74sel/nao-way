###
 # @file steppign_up.h
 #
 # @brief advertise srv to step on an obstacle
 #
 # @ingroup behaviour
 #
 # @author group_c
 # Contact: group_c@tum.c
 #
###

#!/usr/bin/env python

import rospy
import std_srvs
import motion
import almath
import argparse
import math
from naoqi import ALProxy
import sys
import time

from std_msgs.msg import Bool
from behaviour.srv import *
from move_operation import *

class StepUp(object):

    def __init__(self, robotIP, PORT=9559):
        self.motionProxy  = ALProxy("ALMotion", robotIP, PORT)
        self.postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)

        self.srv = rospy.Service("behaviour/step_up", StepHeight, self.handler)
        self.pub = rospy.Publisher("behaviour/success", Bool, queue_size=1)

    def handler(self, req):

        height = req.height.data
	print("height: " + str(height))

        motionProxy = self.motionProxy
        postureProxy = self.postureProxy

        # send NAO to Pose Init
        self.postureProxy.goToPosture("StandInit", 0.4)
        motionProxy.setStiffnesses("Body", 1.0)
        motionProxy.wbEnable(True)

        # ballance on RLeg
        balance_on(motionProxy, "RLeg", 1.5)
        motionProxy.wbEnable(False)

        op = MoveOperation(motionProxy)

        dist_up = 0.019 + height;

        # 1. manipulate LLeg
        print("manipulate LLeg")
        op = arms_right(1, op)
        deltaTf  = almath.Transform(0.0, 0.0, dist_up)
        op.delta_tf("LLeg", deltaTf, 1.5)
        op.execute()

        # 2. move LLeg in front
        print("move LLeg in front")
        deltaTf  = almath.Transform(0.17, 0.0, 0.0)
        deltaTf2 = almath.Transform(0.03, 0.03, -0.02)
        op.delta_tf(["LLeg", "Torso"], [deltaTf, deltaTf2], [1.5, 1.5])
        op.execute()

        # 3. move LLeg little down to stabilize
        print("move LLeg little down to stabilize")
        #deltaTf  = almath.Transform(0.0, 0.0, -0.012)#0.0045
        deltaTf  = almath.Transform(0.0, 0.0, -0.005)#0.0029
        op.delta_tf("LLeg", deltaTf, 0.5)
        op = arms_left(1, op)
        op.execute()

        # TODO!!
        print("move the mass center above the left the leg")
        deltaTfT = almath.Transform(0.11, 0.07, -0.01)*almath.Transform().fromRotY(18.0*almath.TO_RAD)
        op.delta_tf(["Torso"], [deltaTfT], [8])
        op.execute()

        op.move_angle(['RShoulderPitch', 'LShoulderPitch', 'RElbowRoll',
        'RElbowYaw', 'RShoulderRoll'], [-0.9, -0.9, 0.8, -0.9, 0.6], 1)
        op.move_angle(['LAnklePitch', 'LHipPitch'], [-0.2, -0.15], 4)

        print("Lift the right leg")
        op.move_angle(['RKneePitch', 'RAnklePitch'], [1.4, -0.5], 1.5)
        #op.move_angle('RHipRoll', 0.1, 1.5) # block

        op.move_angle(['RHipPitch', 'LHipPitch'], [-1.8, 0.3], 4)

        print("Right leg down")
        op.move_angle(['RAnklePitch'], [0.5], 1.5)

        # stand init
        postureProxy.goToPosture("StandInit", 0.5)

        # publish success
        self.pub.publish(True)

        return StepHeightResponse()


if __name__ == "__main__":
    robotIP=str(sys.argv[1])
    PORT=int(sys.argv[2])
    rospy.init_node('stepping_up_srv_node')

    step_up = StepUp(robotIP, PORT)

    rospy.spin()

