###
 # @file fall_manager.h
 #
 # @brief advertise topic once robot has falled down
 #
 # @ingroup behaviour
 #
 # @author group_c
 # Contact: group_c@tum.c
 #
###

#!/usr/bin/env python
import qi
import rospy
import time
import almath
import sys
from naoqi import ALProxy
from head_manager.srv import *
from std_msgs.msg import Bool


class FallManagerPub(object):
    def __init__(self, app, robotIP, PORT):
        super(FallManagerPub, self).__init__()
        app.start()
        session = app.session
        self.memory = session.service("ALMemory")
        self.subscriber = self.memory.subscriber("robotHasFallen")
        self.subscriber.signal.connect(self.on_robot_has_fallen)

        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)
        self.pub = rospy.Publisher('/fall_state', Bool, queue_size=1)

    def on_robot_has_fallen(self):
        self.pub.publish(1)
        print('in callback ### ')
    
    def run(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep() 


if __name__ == '__main__':
    robotIP=str(sys.argv[1])
    PORT=int(sys.argv[2])

    #init ros
    rospy.init_node('fall_manager_pub')

    # init nao qi framework
    try:
        print('test')
        connection_url = 'tcp://' + robotIP + ':' + str(PORT)
        print connection_url
        app = qi.Application(['FallManagerPub', '--qi-url=' + connection_url])
    except RuntimeError:
        print ('Error connection to nao')
        sys.exit(1)
    
    fall_manager_pub = FallManagerPub(app, robotIP, PORT)
    fall_manager_pub.run()
        

