###
 # @file steppign_over.h
 #
 # @brief advertise srv to step over an obstacle
 #
 # @ingroup behaviour
 #
 # @author group_c
 # Contact: group_c@tum.c
 #
###

#!/usr/bin/env python

import rospy
import std_srvs
import motion
import almath
import argparse
import math
from naoqi import ALProxy
import sys
import time
from behaviour.srv import *
from std_srvs.srv import Empty, EmptyResponse
from std_msgs.msg import Bool

from move_operation import *

class StepUp(object):

    def __init__(self, robotIP, PORT=9559):
        self.motionProxy  = ALProxy("ALMotion", robotIP, PORT)
        self.postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)

        self.srv = rospy.Service("behaviour/step_over", Empty, self.handler)
        self.pub = rospy.Publisher("behaviour/success", Bool, queue_size=1)

    def handler(self, req):

#        height = req.height.data
#	print("height: " + str(height))

        motionProxy = self.motionProxy
        postureProxy = self.postureProxy

        # send NAO to Pose Init
        self.postureProxy.goToPosture("StandInit", 0.4)
        motionProxy.setStiffnesses("Body", 1.0)
        motionProxy.wbEnable(True)

        # ballance on RLeg
        balance_on(motionProxy, "RLeg", 1.5)
        motionProxy.wbEnable(False)

        op = MoveOperation(motionProxy)

        #dist_up = 0.019 + height;

    ###################### Step I ##########################
        # Left Mass Center
        print("Rotate left leg 90 degree and lift it up")
        #op = arms_right(1, op)
        deltaTfL = almath.Transform(0.0, 0.0, 0.025)*almath.Transform().fromRotZ(90.0*almath.TO_RAD)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        deltaTfT = almath.Transform(0.0, 0.0, 0.0)*almath.Transform().fromRotY(5*almath.TO_RAD)
        op.delta_tf(["LLeg", "RLeg", "Torso"], [deltaTfL, deltaTfR, deltaTfT], 2)
        op.execute()
        time.sleep(0.5)

        print("Put left foot near the bar")
        deltaTfL  = almath.Transform(0.02, -0.06, 0)
        deltaTfR  = almath.Transform(0.0, 0.0, 0.0)
        deltaTfT  = almath.Transform(0.02, -0.04, -0.03)
        op.delta_tf(["LLeg", "RLeg", "Torso"], [deltaTfL, deltaTfR, deltaTfT], 4)
        op.execute()
        deltaTfL  = almath.Transform(0.0, 0.0, -0.02)
        op.delta_tf(["LLeg"], [deltaTfL], 2)
        op.execute()
        time.sleep(0.5)

        print("Get the mass center to the left leg by adjusting the position and orientation of torso")
        deltaTfT  = almath.Transform(0.12, 0.02,  -0.02)*almath.Transform().fromRotY(25.0*almath.TO_RAD) # z = -0.02
        op.delta_tf(["Torso"], [deltaTfT], 4)
        op.execute()
        op.move_angle(['LHipPitch', 'LKneePitch', 'RKneePitch'], [-0.2, -0.15, 0.2], 2)


        print("Raise the right leg")
        op.move_angle('RKneePitch', 0.6, 1.5)
        op.move_angle('RHipRoll', 0.25, 1.5)
        op.move_angle(['RHipPitch'], [-1.5], 4)

        print("execute the stepping over of right leg")
        op.move_angle(['RKneePitch','RAnklePitch','LKneePitch','LAnkleRoll'], [-0.9, 1.8, 0.45, 0.1], 4)
        deltaTfL = almath.Transform(0.1, 0.0, 0.0)
        deltaTfT = almath.Transform(0.03, 0.0, 0.02)
        deltaTfR = almath.Transform(-0.03, 0.0, -0.055)
        op.delta_tf(["Torso", "RLeg"], [deltaTfT, deltaTfR], 5)
        op.execute()

    ###################### Step II ##########################

        # Right Mass Center
        print("lifting torso")
        deltaTfT  = almath.Transform(0.01, -0.02, 0.035)#*almath.Transform().fromRotY(25.0*almath.TO_RAD)
        deltaTfL = almath.Transform(0.0, 0.0, 0.0)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        op.delta_tf(["LLeg","Torso", "RLeg"], [deltaTfL, deltaTfT, deltaTfR], 3)
        op.execute()
        time.sleep(0.5)

        print("Move the mass center along to the right leg")
        deltaTfT  = almath.Transform(0.05, -0.11, -0.03)

        deltaTfL = almath.Transform(0.0, 0.0, 0.0)#*almath.Transform().fromRotZ(-30.0*almath.TO_RAD)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        op.delta_tf(["LLeg","Torso", "RLeg"], [deltaTfL, deltaTfT, deltaTfR], 7)
        op.execute()
        time.sleep(0.5)

        print("Move the mass center above the right leg")
        op.move_angle(['RHipRoll', 'RHipPitch'], [-0.2, -0.4], 6)
        time.sleep(0.5)

        print("Raise Left Leg")
        op.move_angle(['LHipPitch', 'LKneePitch', 'LAnklePitch'], [-0.45, 1.6, -0.9], 4)
        time.sleep(0.5)

        print("Rotating the Left Leg")
        deltaTfL = almath.Transform(0.06, -0.02, 0.02)*almath.Transform().fromRotZ(-86.0*almath.TO_RAD)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)
        #deltaTfT = almath.Transform(0.05, -0.03, 0.03)
        deltaTfT = almath.Transform().fromRotY(-15.0*almath.TO_RAD)
        op.delta_tf(["LLeg", "RLeg", "Torso"], [deltaTfL, deltaTfR, deltaTfT], 6)
        op.execute()
        time.sleep(0.5)

        print("Adjusting the Left foot horizontal to the ground")
        op.move_angle(['LHipPitch'], [-1.2], 3)
        time.sleep(0.5)

        print("Left leg touching the ground, Torso to the left")
        deltaTfL = almath.Transform(0.0, 0.0, -0.05)
        deltaTfR = almath.Transform(0.0, 0.0, 0.0)

        op.delta_tf(["LLeg", "RLeg"], [deltaTfL, deltaTfR], 2)
        op.execute()
        postureProxy.goToPosture("StandInit", 0.3)

    ###################### Step III ##########################

        # publish success
        self.pub.publish(True)

        return EmptyResponse()


if __name__ == "__main__":
    robotIP=str(sys.argv[1])
    PORT=int(sys.argv[2])
    rospy.init_node('stepping_up_srv_node')

    step_up = StepUp(robotIP, PORT)

    rospy.spin()


