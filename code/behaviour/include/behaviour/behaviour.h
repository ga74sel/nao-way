/**
 * @file behaviour.h
 *
 * @brief provide ros services for obstacle approching
 *
 * @ingroup behaviour
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */

#ifndef BEHAVIOUR_H
#define BEHAVIOUR_H

#include <vector>
#include <math.h>

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Pose2D.h>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include <obstacle_msgs/detect.h>
#include <head_manager/MoveJoints.h>
#include <walk_manager/WalkPose.h>
#include <behaviour/Approach.h>

namespace behaviour {

/*
 * Save Information about obstacle approach points
 */
class Obstacle
{
public:
    Eigen::Affine2d TO_B;                       // 2D transformation obstacle wrt base
    Eigen::Vector2d X_B;                        // Marker position wrt base frame
    Eigen::Matrix<double,3,4> Pts_O;            // Approach points in obstacle frame
    Eigen::Matrix<double,3,4> Pts_B;            // Approach points in base frame
};


/**
 * Implementation obstacle approching behaviour
 *
 * continously checks transformation between observed obstacle and robot base.
 * Moves until a desired transformation in front of the obstacle is reached
 *
 */
class Behaviour {

public:
    Behaviour();
    void update();

private:
    /// AUXILIARY FUNCTIONS
    void pub_success(bool success);
    void walk_to(geometry_msgs::Pose2D &p);
    void turn_head(float yaw, float pitch, float time);

    void compute_goal(geometry_msgs::Pose2D &p);
    double norm(const geometry_msgs::Pose2D& p);

    /// CALLBACK
    void timer_cb(const ros::TimerEvent&);
    bool start_approaching_cb(behaviour::Approach::Request &req, behaviour::Approach::Response &res);

    void state_transition();
    bool transform_obstacle(obstacle_msgs::detectResponse &res);

private:

    ros::NodeHandle nh;

    // Publisher
    ros::Publisher success_pub;

    // ServiceServer
    ros::ServiceServer start_srv;

    // ServiceClient
    ros::ServiceClient detect_client;
    ros::ServiceClient head_client;
    ros::ServiceClient walk_client;

    // Timer
    ros::Timer timer;
    bool timer_flag;

    // Transforms Tf
    tf::TransformListener listener;
    std::string robot_frame;

    // State Machine
    enum State {IDLE, WAIT_TO_TURN, TURN_HEAD, SEARCH_MARKER, ADJUST_TH, ADJUST_X, ADJUST_Y, WALKING_DONE };
    State state, next_state;

    bool is_busy;
    Obstacle obstacle;
    int obstacle_id;
    int head_turn_count;
    geometry_msgs::Pose2D goal;
};

}
#endif // BEHAVIOUR_H
