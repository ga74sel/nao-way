/**
 * @file localization.h
 *
 * @brief localize the robot base on a goal aruco marker
 *
 * @ingroup obstacle_msgs
 *
 * @author group_c
 * Contact: group_c@tum.c
 *
 */

#ifndef LOCALIZATION_H
#define LOCALIZATION_H

#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Pose2D.h>

#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include <aruco/aruco.h>
#include <aruco/cvdrawingutils.h>
#include <aruco/markerdetector.h>

// Eigen
#include <Eigen/Dense>
#include <eigen_conversions/eigen_msg.h>

#include <cv.h>
#include <opencv2/core/eigen.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <vector>
#include <math.h>


namespace enc = sensor_msgs::image_encodings;

typedef std::vector<aruco::Marker> ArucoMarkers;

/**
* Localization of robot
*
* Detects a goal aruco marker and extracts tranformation wrt to robot_base
* and broadcast a new transformation /map wrt to /odom that can be used for navigation
*
* Localization is available in the ros tf tree after localize_srv is called
*/
class Localization {

public:
    Localization();

    /**
    * @brief update marker positions
    */
    void update();

private:
    /**
    * @brief extract marker pose wrt given frame
    * @param img image to analyse
    * @param frame frame wrt pose is computed to
    * @param draw draw into image
    */
    bool extract_marker_pose(cv::Mat& img, Eigen::Affine3d& TM_0, double& height, bool draw = false);

    /**
    * @brief compute the transformation of odom wrt map
    * @param TM_0 transformation /map to /base
    * @param TO_map transformation /odom to map
    */
    void compute_map_odom(const Eigen::Affine3d& TM_0, Eigen::Affine3d& TO_map);

    /**
    * @brief compute the transformation of odom wrt map
    * @param TM_0 transformation /map to /base
    * @param TO_map transformation /odom to map
    */
    void broadcast_tf(const Eigen::Affine3d &TO_Map, double height);

    /// @brief topic callback img top
    void image_cb(const sensor_msgs::ImageConstPtr& msg);

    /// @brief service handle to detect obstacles
    bool localize_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);

private:

    ros::NodeHandle nh_;

    image_transport::ImageTransport it_;
    cv::Mat image;
    bool img_updated;
    int img_cnt;

    std::string base_frame, frame, odom;

    Eigen::Affine3d TO_Map; // transformation odom wrt map
    double height;

    // Camera Parameters
    aruco::CameraParameters camera_parameters;
    aruco::MarkerDetector aruco_detector;
    float marker_size;

    // Subscriber
    image_transport::Subscriber image_sub_;

    // Service Server
    ros::ServiceServer localize_srv;

    // tf Transform
    tf::TransformListener listener;
    // tf Broadcaster
    tf::TransformBroadcaster br;

    bool map_exists;
};

#endif // LOCALIZATION_H
