#include "localization/localization.h"
#include "obstacle_msgs/math.h"

Localization::Localization() : it_(nh_), img_updated(false), map_exists(false),
    odom("/odom"),
    base_frame("/base_footprint"),
    frame("/CameraTop_optical_frame"),
    img_cnt(0)
{
    // Service Server
    localize_srv = nh_.advertiseService("/localization/localize", &Localization::localize_srv_cb, this);

    // Subscriber
    image_sub_ = it_.subscribe("/nao_robot/camera/top/camera/image_raw", 500, &Localization::image_cb, this);

    // camera prameters
    cv::Mat dist = (cv::Mat_<double>(4, 1) << -0.0545211535376379, 0.0691973423510287, -0.00241094929163055, -0.00112245009306511);
    cv::Mat cameraK = (cv::Mat_<double>(3, 3) << 556.845054830986, 0.0, 309.366895338178, 0.0, 555.898679730161, 230.592233628776, 0.0, 0.0, 1.0);
    camera_parameters.setParams(cameraK,dist,cv::Size(640,480));
    marker_size = 0.182f;
}

void Localization::update() {

    // 1. visualize marker
    if(img_updated) {

        if(img_cnt==10) {
            img_cnt = 0;

            Eigen::Affine3d TM_0;
            bool success = extract_marker_pose(image, TM_0, height, true);

            // do map update here only initially
            if(!map_exists) {
                if(success)
                    compute_map_odom( TM_0, TO_Map );
            }
        }
        img_cnt++;
    }

    // 2. broadcast /map tf
    broadcast_tf(TO_Map, height);
}

bool Localization::extract_marker_pose(cv::Mat& img, Eigen::Affine3d& TM_0, double& height, bool draw)
{
    bool success = false;
    Eigen::Affine3d TM_C;   // marker wrt camera
    Eigen::Affine3d TC_0;   // camera wrt base

    // get transformation: camera wrt base
    tf::StampedTransform transform;
    try {
        listener.lookupTransform(base_frame, frame, ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("Localization::extract_marker_pose: no transformation");
        return 0;
    }
    tf::transformTFToEigen(transform, TC_0);

    // detect markers
    std::vector<aruco::Marker> aruco_markers;
    aruco_detector.detect(img, aruco_markers);

    // compute goal wrt base
    for( size_t i = 0; i < aruco_markers.size(); ++i )
    {
        if(aruco_markers[i].id != 0)
            continue;

        aruco_markers[i].calculateExtrinsics(marker_size, camera_parameters, false);
        math::cvpose2eigen(aruco_markers[i].Tvec, aruco_markers[i].Rvec, TM_C);
        TM_0 = TC_0 * TM_C.rotate(Eigen::AngleAxisd(M_PI/2, Eigen::Vector3d::UnitY()));

        Eigen::Vector3d t = TM_0.translation(); height = t[2]; t[2] = 0.0;
        Eigen::Vector3d ea = TM_0.rotation().eulerAngles(2, 1, 0);
        ea[0] = (ea[0] > M_PI/2) ? ea[0] - M_PI : ea[0];
        TM_0 = Eigen::Translation3d(t) * Eigen::AngleAxisd(ea[0], Eigen::Vector3d::UnitZ());
        success = true;

        if( draw ) {
             aruco_markers[i].draw(img, cv::Scalar(0,0,255), 2);
             aruco::CvDrawingUtils::draw3dAxis(img, aruco_markers[i], camera_parameters);
        }
    }
    return success;
}

// ---------
// Tf
// ---------

void Localization::compute_map_odom(const Eigen::Affine3d& TM_0, Eigen::Affine3d& TO_Map) {

    Eigen::Affine3d T0_O;   // base wrt odom

    // get transformation: base_link wrt odom
    tf::StampedTransform transform;
    try {
        listener.lookupTransform(odom, base_frame, ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("Localization::extract_marker_pose: no transformation");
        return;
    }
    tf::transformTFToEigen(transform, T0_O);

    // compute map tranformation
    TO_Map = ( T0_O * TM_0 ).inverse();
    map_exists = true;
}

void Localization::broadcast_tf(const Eigen::Affine3d& TO_Map, double height) {
    if(map_exists) {
        std::vector<tf::StampedTransform> tf_transformations;
        tf_transformations.resize(2);

        tf::Transform tf_transform;
        tf::transformEigenToTF( TO_Map, tf_transform );

        tf_transformations[0].setData( tf_transform );
        tf_transformations[0].stamp_ = ros::Time::now();
        tf_transformations[0].frame_id_ = "/map";
        tf_transformations[0].child_frame_id_ = "/odom";

        tf::Vector3 origin = tf::Vector3(0, 0, height);
        tf_transform.setIdentity();
        tf_transform.setOrigin( origin );
        tf_transformations[1].setData( tf_transform );
        tf_transformations[1].stamp_ = ros::Time::now();
        tf_transformations[1].frame_id_ = "/map";
        tf_transformations[1].child_frame_id_ = "/goal";

        br.sendTransform(tf_transformations);
    }
}

void Localization::image_cb(const sensor_msgs::ImageConstPtr& msg) {

    cv_bridge::CvImageConstPtr cvImagePointer;
    try {
        cvImagePointer = cv_bridge::toCvShare(msg, enc::BGR8);
        image = cvImagePointer->image.clone();
        img_updated = true;
    }
    catch (cv_bridge::Exception& except) {
        ROS_ERROR("cv_bridge exception: %s", except.what());
        return;
    }
}

bool Localization::localize_srv_cb(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {

    bool success;
    Eigen::Affine3d TM_0;

    // try marker detection 3 times
    for(int i=0; i<3; i++) {
        success = extract_marker_pose(image, TM_0, height, true);
        if(success) {
            compute_map_odom( TM_0, TO_Map );
            break;
        }
    }
    return success;
}

