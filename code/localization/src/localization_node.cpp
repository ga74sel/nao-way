#include "ros/ros.h"
#include "localization/localization.h"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "localization_node");
    ROS_INFO_STREAM("localization_node...");

    Localization localizer;
    ros::Rate loop_rate(20);

    while(ros::ok())
    {
        localizer.update();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
