#include "visualizer/visualizer.h"

using namespace  visualizer;

const Eigen::Vector3d Eigen_zAxis = (Eigen::Vector3d() << 0, 0, 1).finished();
const tf::Vector3 tf_zAxis = tf::Vector3(0.0, 0.0, 1.0);

Visualizer::Visualizer()
    : maker_updated(false),
      map_frame("/base_footprint"),
      step_over_height(0.027),
      setp_up_height(0.048)
{
    // Publisher
    marker_pub = nh.advertise<visualization_msgs::MarkerArray>( "visualizer/marker", 1);

    // Subscriber
    obstacle_sub = nh.subscribe("/obstacledetector/obstacleposes_static", 1, &Visualizer::obstacle_callback, this);

    clear_srv = nh.advertiseService("/visualizer/clear", &Visualizer::clear_handler, this);

    // setup marker
    max_marker = 10;
    for( int i = 0; i < max_marker; i++) {
        marker_array.markers.push_back( init_marker(i, 0.0, 0.8, 0.0) );
    }
}

visualization_msgs::Marker Visualizer::init_marker(int id, double r, double g, double b) {

    visualization_msgs::Marker marker;

    marker.header.frame_id = map_frame;
    marker.header.stamp = ros::Time();
    marker.ns = "features";
    marker.id = id;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 0.0;
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;

    return marker;
}

void Visualizer::update()
{
    if( maker_updated ) {
        maker_updated = false;
        visualize_marker();
    }
}

void Visualizer::update_markers(const Obstacle& obstacle, visualization_msgs::Marker& marker)
{
    marker.pose = obstacle.pose;
    marker.scale = obstacle.scale;
    marker.color.a = 0.6;

    //std::cout << "HEIGHT== " << obstacle.height << std::endl;
    if( obstacle.height > setp_up_height ) {
        marker.color.r = 1.0; marker.color.g = 0.0; marker.color.b = 0.0;
    }
    else if( obstacle.height < setp_up_height ) {
        if( obstacle.height < step_over_height ) {
            marker.color.r = 0.0; marker.color.g = 1.0; marker.color.b = 1.0;
        }
        else {
            marker.color.r = 0.0; marker.color.g = 1.0; marker.color.b = 0.0;
        }
    }
}

void Visualizer::visualize_marker() {

    // 1. visualize
    for( auto it = obstacle_map.begin(); it != obstacle_map.end(); ++it) {
        if( it->first < max_marker ) {
            update_markers(it->second, marker_array.markers[it->first]);
        }
    }

    // 2. send
    marker_pub.publish(marker_array);
}

// ---------
// CALLBACKS
// ---------

void Visualizer::obstacle_callback(obstacle_msgs::obstacleposesConstPtr msg)
{
    // construct obstacle markers
    auto& obstacleposes = msg->obstacleposes;
    std::string obstacle_frame = msg->header.frame_id;

    // get the transformation into map frame
    Eigen::Affine3d TO_M, Tobst, T;

    // get transformation obstacle_frame wrt map_frame
    tf::StampedTransform transform;
    listener.waitForTransform(map_frame, obstacle_frame, ros::Time(0), ros ::Duration(0.2));
    try {
        listener.lookupTransform(map_frame, obstacle_frame, ros::Time(0), transform);
    } catch(tf::TransformException &ex) {
        ROS_ERROR_STREAM("ObstacleDetector::extract_marker_pose: no transformation");
        return;
    }
    tf::transformTFToEigen(transform, TO_M);

    // transform every obstacle footprint
    for( int i = 0; i < obstacleposes.size(); ++i) {
        // transform into map_frame
        auto& obstacle = obstacleposes[i];
        int id = obstacleposes[i].id.data;

        Eigen::Vector3d position;
        position << obstacle.pose.x, obstacle.pose.y, 0.0;
        Tobst = Eigen::Translation3d(position) * Eigen::AngleAxisd(obstacle.pose.theta, Eigen_zAxis);
        T = TO_M * Tobst;

        // obstacle center point
        obstacle_map[id].pose.position.x = T.translation()[0];
        obstacle_map[id].pose.position.y = T.translation()[1];
        obstacle_map[id].pose.position.z = obstacle.height.data / 2.0;

        // obstacle scale
        obstacle_map[id].scale.x = 2.0 * std::abs(obstacle.footprint.points[0].x);
        obstacle_map[id].scale.y = 2.0 * std::abs(obstacle.footprint.points[0].y);
        obstacle_map[id].scale.z = obstacle.height.data;

        // obstacle orientation in xy plane
        Eigen::Rotation2D<double> R(0);
        R.fromRotationMatrix( T.linear().topLeftCorner<2,2>() );

        tf::Quaternion quad = tf::Quaternion(tf_zAxis, R.angle());
        obstacle_map[id].pose.orientation.x = quad.x();
        obstacle_map[id].pose.orientation.y = quad.y();
        obstacle_map[id].pose.orientation.z = quad.z();
        obstacle_map[id].pose.orientation.w = quad.w();

        obstacle_map[id].height = obstacle.height.data;
    }
    maker_updated = true;
}


bool Visualizer::clear_handler(std_srvs::EmptyRequest &req, std_srvs::EmptyResponse &res)
{
    for( auto it = obstacle_map.begin(); it != obstacle_map.end(); ++it) {
        if( it->first < max_marker ) {
            marker_array.markers[it->first].color.a = 0.0;
        }
    }
    obstacle_map.clear();
    return true;
}

