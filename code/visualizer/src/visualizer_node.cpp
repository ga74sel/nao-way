#include "ros/ros.h"
#include "visualizer/visualizer.h"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "visualizer_node");
    ROS_INFO_STREAM("visualizer_node...");

    visualizer::Visualizer vis;
    ros::Rate loop_rate(5);

    while(ros::ok())
    {
        vis.update();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
