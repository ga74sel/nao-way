#ifndef VISUALIZER_H
#define VISUALIZER_H

#include <ros/ros.h>

#include <geometry_msgs/Pose2D.h>

#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include <visualization_msgs/MarkerArray.h>

#include <obstacle_msgs/obstacleposes.h>
#include <obstacle_msgs/obstacle.h>
#include <std_srvs/Empty.h>

namespace visualizer
{

/*
 * Deciption of marker
 */
class Obstacle {
public:
    geometry_msgs::Pose pose;
    geometry_msgs::Vector3 scale;
    float height;
};
typedef std::map<int, Obstacle> ObstacleMap;

/*
 * Visualize Obstacles by 3d shapes
 */
class Visualizer
{

public:
    Visualizer();
    void update();

private:

    ros::NodeHandle nh;

    // Publisher
    ros::Publisher marker_pub;

    // Subscriber
    ros::Subscriber obstacle_sub;

    // Service
    ros::ServiceServer clear_srv;

    // Marker
    int max_marker;
    visualization_msgs::MarkerArray marker_array;
    visualization_msgs::Marker init_marker(int id, double r, double g, double b);
    void update_markers(const Obstacle& obstacle, visualization_msgs::Marker& marker);
    void visualize_marker();

    // Obstacles
    ObstacleMap obstacle_map;
    bool maker_updated;

    // tf Transform
    tf::TransformListener listener;
    std::string map_frame;

    // CALLBACKS
    void obstacle_callback(obstacle_msgs::obstacleposesConstPtr msg);

    bool clear_handler(std_srvs::EmptyRequest &req, std_srvs::EmptyResponse &res);

    float step_over_height;
    float setp_up_height;

};

}

#endif // VISUALIZER_H
